﻿using UnityEngine;

namespace CoroutineHandling
{
    public interface IRoutinable
    {
        void OnRoutinableStart();
        void OnAction();
    }   
}