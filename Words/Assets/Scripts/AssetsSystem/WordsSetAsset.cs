﻿using GoogleSheetsToUnity;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AssetsSystem
{
    public interface ISynchronizeableAsset
    {        
        void Synchronize(GstuSpreadSheet sheet, Properties name);
    }

    public interface IUniqueObject
    {
        int Id { get; }
    }

    [System.Serializable, ProtoContract(SkipConstructor = true)]
    public struct Word
    {
        [ProtoMember(1)]
        public string english;
        [ProtoMember(2)]
        public string russian;
    }

    [System.Serializable, ProtoContract(SkipConstructor = true)]
    public class DateTimeSerializeable
    {
        [ProtoMember(1)]
        public int year;
        [ProtoMember(2)]
        public int month;
        [ProtoMember(3)]
        public int day;
        [ProtoMember(4)]
        public int hour;
        [ProtoMember(5)]
        public int minute;
        [ProtoMember(6)]
        public int second;
        [ProtoMember(7)]
        public int millisecond;

        public DateTimeSerializeable(System.DateTime dateTime)
        {
            year = dateTime.Year;
            month = dateTime.Month;
            day = dateTime.Day;
            hour = dateTime.Hour;
            minute = dateTime.Minute;
            second = dateTime.Second;
            millisecond = dateTime.Millisecond;
        }

        public System.DateTime ToDateTime()
        {
            return new System.DateTime(year, month, day, hour, minute, second, millisecond);
        }
    }

    [System.Serializable, ProtoContract(SkipConstructor = true)]
    public class WordsSetAsset : ISynchronizeableAsset, IUniqueObject
    {
        [ProtoMember(1)]
        private string name;
        public string Name { get { return name; } }
        [ProtoMember(2)]
        private int id;
        public int Id { get { return id; } }
        [ProtoMember(3)]
        private List<Word> words;
        public List<Word> Words { get { return words; } }
        [ProtoMember(4)]
        private bool isChoosed;
        public bool IsChoosed { get { return isChoosed; } }

        private static string namesRowTitle = "English";

        public void Synchronize(GstuSpreadSheet sheet, Properties property)
        {
            if (words != null)
            {
                throw new System.Exception("You try to sync list, but some values already exists!");
            }

            name = property.title;
            id = property.sheetId;

            words = new List<Word>();

            var names = GetNames(sheet, namesRowTitle);

            foreach(var entry in names)
            {
                var english = entry;
                var russian = sheet[entry, "Russian"].value;
                words.Add(new Word()
                {
                    english = english,
                    russian = russian
                });

                //Debug.LogFormat("Synchronize {0}, {1}", english, russian);
            }

            Debug.LogFormat("Synchronize complete! {0}", words.Count);
        }

        private List<string> GetNames(GstuSpreadSheet sheet, string namesRowTitle)
        {
            var dict = sheet.rows.secondaryKeyLink;

            if (!dict.ContainsKey(namesRowTitle))
            {
                throw new System.Exception(string.Format("Can't find asset names row title in the table! {0}", namesRowTitle));
            }

            List<string> result = new List<string>();

            foreach (var entry in dict)
            {
                if (entry.Key != namesRowTitle)
                {
                    //Debug.LogFormat("GetNames {0}", entry.Key);
                    result.Add(entry.Key);
                }
            }

            return result;
        }

        public void SetChoosed(bool isChoosed)
        {
            this.isChoosed = isChoosed;
            Debug.LogFormat("{0}", this.isChoosed);
        }
    }

    [System.Serializable, ProtoContract(SkipConstructor = true)]
    public class FileAsset
    {
        [ProtoMember(1)]
        private string fileId;
        public string FileId { get { return fileId; } }
        [ProtoMember(2)]
        private List<WordsSetAsset> wordsSetList;
        public List<WordsSetAsset> WordsSetList { get { return wordsSetList; } }
        [ProtoMember(3)]
        private DateTimeSerializeable lastModified;
        public DateTimeSerializeable LastModified { get { return lastModified; } }

        private Dictionary<int, WordsSetAsset> wordsSetDict;
        
        public FileAsset(string fileId, DateTime lastRevision)
        {
            this.fileId = fileId;
            wordsSetList = new List<WordsSetAsset>();
            wordsSetDict = new Dictionary<int, WordsSetAsset>();
            lastModified = new DateTimeSerializeable(lastRevision);
        }

        public void Initialize(DateTime lastRevision)
        {
            wordsSetDict = new Dictionary<int, WordsSetAsset>();

            foreach (var entry in wordsSetList)
            {
                var key = entry.Id;
                //Debug.LogFormat("{0}, {1}", key, wordsSetList.Count);

                if (!wordsSetDict.ContainsKey(key))
                {
                    wordsSetDict.Add(entry.Id, entry);
                }
                else
                {
                    throw new Exception(string.Format("Entry with key {0} already exists in dict", entry.Id));
                }
            }

            //Debug.LogFormat("Words count: {0}", wordsSetList.First().Words.Count);

            lastModified = new DateTimeSerializeable(lastRevision);
        }

        public int WordsCount { get { return wordsSetList.First().Words.Count; } }

        public void AddWordSet(WordsSetAsset wordSet)
        {
            bool isExists = false;
            int equalIndex = 0;
            for (var i = 0; i < wordsSetList.Count; i++)
            {
                var entry = wordsSetList[i];
                if (entry.Id == wordSet.Id)
                {
                    isExists = true;
                    equalIndex = i;
                }
            }

            Debug.LogFormat("AddWordSet {0}, {1}", wordSet.Id, isExists);
            if (!isExists)
            {
                wordsSetList.Add(wordSet);
            }
            else
            {
                wordsSetList[equalIndex] = wordSet;
                Debug.LogFormat("Assets set with id {0} already exists in list", wordSet.Id);
            }            
        }

        public WordsSetAsset GetAssetById(int id)
        {
            WordsSetAsset result;

            var isSuccess = wordsSetDict.TryGetValue(id, out result);

            if (isSuccess)
            {
                return result;
            }
            else
            {
                throw new System.Exception(string.Format("Can't find any assets for id {0}!", id));
            }
        }
    }
}