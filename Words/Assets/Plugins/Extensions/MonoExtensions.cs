﻿using UnityEngine;

public static class MonoExtensions
{
    #region SAFE_DESTROY

    public static TObject SafeDestroy<TObject>(this TObject obj)
        where TObject : UnityEngine.Object
    {
        if (UnityEngine.Application.isEditor)
            UnityEngine.Object.DestroyImmediate(obj);
        else
            UnityEngine.Object.Destroy(obj);

        return null;
    }

    public static TObject SafeDestroyByComponent<TObject>(this TObject component) where TObject : UnityEngine.Component
    {
        if (component != null)
            SafeDestroy(component.gameObject);
        return null;
    }

    #endregion

    #region SAFE_SETACTIVE

    public static void SetActiveSafe(this GameObject obj, bool value)
    {
        if(obj.activeSelf != value)
        {
            obj.SetActive(value);
        }
    }

    #endregion

    public static bool IsNan(this Vector3 origin)
    {
        return float.IsNaN(origin.x) || float.IsNaN(origin.y) || float.IsNaN(origin.z);
    }
}
