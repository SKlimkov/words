﻿namespace DelegatesHandling
{
    public delegate void VoidDelegate();

    public class VoidEventHolder : IEventHolder<VoidDelegate>, IEventExecutor<VoidDelegate>
    {
        private VoidDelegate onEventHandler;

        public void AddListener(VoidDelegate handler)
        {
            onEventHandler += handler;
        }

        public void RemoveListener(VoidDelegate handler)
        {
            if (onEventHandler != null)
            {
                onEventHandler -= handler;
            }
        }

        public void ClearListeners()
        {
            onEventHandler = null;
        }

        public void Execute()
        {
            if (onEventHandler != null)
            {
                onEventHandler();
            }
        }

        //public VoidDelegate Event { get { return onEventHandler; } }
    }
}