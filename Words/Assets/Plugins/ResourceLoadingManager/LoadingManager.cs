﻿using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;

namespace ResourceLoading
{
    public abstract class LoadingManager
    {
        #region NESTED_TYPES

        public struct Config
        {
            public string id;
            public string uri;
        }

        #endregion
    }

    public abstract class LoadingManager<TObject> : LoadingManager
        where TObject : Object
    {
        private Dictionary<string, TObject> m_Assets = new Dictionary<string, TObject>();

        public virtual TObject LoadObject(Config config)
        {
            TObject result;
            var isSuccess = m_Assets.TryGetValue(config.id, out result);

            if (isSuccess)
            {
                return result;
            }

            return AddObject(config);
        }

        public virtual async UniTask<TObject> LoadObjectAsync(Config config)
        {
            TObject result;
            var isSuccess = m_Assets.TryGetValue(config.id, out result);

            if (isSuccess)
            {
                return result;
            }
            else
            {
                result = await AddAsyncObject(config);
            }


            return result;
        }

        public bool UnloadObjectSafe(Config config)
        {
            var isSuccess = m_Assets.ContainsKey(config.id);

            if (isSuccess)
            {
                UnloadObject(config);
            }

            return isSuccess;
        }

        public void UnloadObject(Config config)
        {
            TObject asset;
            var isSuccess = m_Assets.TryGetValue(config.id, out asset);

            if (isSuccess)
            {
                Resources.UnloadAsset(asset);
                m_Assets.Remove(config.id);
            }
            else
            {
                throw new System.Exception(string.Format("Can't find object with id {0}", config.id));
            }            
        }

        public virtual void Clear()
        {
            foreach (var entry in m_Assets)
            {
                Resources.UnloadAsset(entry.Value);
            }
            m_Assets.Clear();
        }

        private TObject AddObject(Config config)
        {
            TObject result = Resources.Load<TObject>(config.uri);
            m_Assets[config.id] = result;
            return result;
        }

        private async UniTask<TObject> AddAsyncObject(Config config)
        {
            var request = await Resources.LoadAsync<TObject>(config.uri);
            if (request != null)
            {
                var result = request as TObject;
                m_Assets[config.id] = result;
                return result;
            }
            else
            {
                throw new System.Exception(string.Format("Can't load asset for {0} id", config.id));
            }            
        }
    }

    public class Prefabs
    {
        public class Ui
        {
            public static LoadingManager.Config WorkSheetViewConfig = new LoadingManager.Config() { id = "WorkSheetViewPrefab", uri = "Prefabs/Windows/WorkSheetView" };
        }        
    }

    public class PrefabLoadingManager : LoadingManager<GameObject> { }

    public class ScriptableObjectManager : LoadingManager<ScriptableObject> { }

    public class SpriteManager : LoadingManager<Sprite> { }

    public class Texture2DManager : LoadingManager<Texture2D> { }

    public class FontManager : LoadingManager<Font> { }
}