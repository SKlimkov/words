﻿using CoroutineHandling;
using System;
using System.Collections;
using UnityEngine;
using Zenject;

namespace UniRx
{
    public class RoutineHelper
    {
        #region LAZY_CALL

        private static ICoroutineService m_CoroutineService;

        [Inject]
        private void ResolveDepenedencies(DiContainer container)
        {
            m_CoroutineService = container.ResolveId<ICoroutineService>("CoroutineService");
        }

        public static void GetLazyObservable<TParam>(FrameCountType type, Action<TParam> action, TParam param, bool publishEveryYield = false)
        {
            Observable.WhenAll(Observable.FromMicroCoroutine(TestAction, publishEveryYield, type)).Subscribe(_ => { action(param); }).AddTo(m_CoroutineService.MonoInstance);
        }

        private static IEnumerator TestAction()
        {
            Debug.LogFormat("-------------- TestAction 1 {0}", Time.time);
            yield return null;
            Debug.LogFormat("-------------- TestAction 1 {0}", Time.time);
        }

        #endregion
    }
}