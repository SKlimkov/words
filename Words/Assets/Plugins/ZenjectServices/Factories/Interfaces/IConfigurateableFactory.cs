﻿using CustomFactory;
using UnityEngine;

public interface IConfigurateableFactory<TConfig, TConfigurateable>
    where TConfigurateable : class, IConfigurateable<TConfig>
{
    TConfigurateable Create(TConfig config, GameObject prefab = null);
}