﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleRemoteSync
{
    public interface ISheetSettings
    {
        string SheetId { get; }
    }

    [CreateAssetMenu(fileName = "New SheetSettings", menuName = "Sheet Settings", order = 52)]
    public class SheetSettings : ScriptableObject, ISheetSettings
    {
        #region SHEET

        [SerializeField]
        private string associatedSheet = "1S8l6oYcy1V3cjE8UNiRKcXQuV3xdxXD4JPH_9czdDUg";

        public string SheetId { get { return associatedSheet; } }

        #endregion
    }
}