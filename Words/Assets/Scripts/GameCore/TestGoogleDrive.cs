﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityGoogleDrive;

public abstract class AdaptiveWindowGUI : MonoBehaviour
{
    public float LeftMargin = 150f, TopMargin, RightMargin, BottomMargin;

    private Rect windowRect;
    private string className;

    protected abstract void OnWindowGUI(int windowId);

    protected virtual string GetWindowTitle()
    {
        return className;
    }

    protected virtual int GetWindowId()
    {
        return 0;
    }

    protected virtual void Awake()
    {
        className = GetType().Name;
    }

    protected virtual void OnGUI()
    {
        GUILayout.Window(GetWindowId(), windowRect, OnWindowGUI, GetWindowTitle());
    }

    protected virtual void Update()
    {
        SetWindowRect();
    }

    protected virtual void SetWindowRect()
    {
        windowRect = new Rect(LeftMargin, TopMargin,
            Screen.width - LeftMargin - RightMargin,
            Screen.height - TopMargin - BottomMargin);
    }
}

public class TestGoogleDrive : AdaptiveWindowGUI
{
    private async void Start() => info = await UpdateInfo((result) => 
    {
        if (request != null && request.IsDone && !request.IsError)
        {
            //isUpdated = true;
            Debug.LogFormat("Gotcha! {0}", result);
            foreach (var entry in result.Revisions)
            {
                Debug.LogFormat("{0}, {1}", entry.Id, entry.ModifiedTime.ToLocalTime());
            }
        }

        Debug.LogFormat("UpdateInfo Complete");
    });

    protected override void OnWindowGUI(int windowId) { }

    //private bool isUpdated;

    protected override void Awake()
    {
        base.Awake();

        //isUpdated = false;
    }

    protected override void Update()
    {
        base.Update();

        /*if (!isUpdated)
        {
            if (request != null && request.IsDone && !request.IsError)
            {
                isUpdated = true;
                Debug.LogFormat("Gotcha!");
                foreach(var entry in info.Revisions)
                {
                    Debug.LogFormat("{0}, {1}", entry.Id, entry.ModifiedTime.ToLocalTime());
                }                
            }
        }*/
    }

    FileRevisionInfo.GetRequest request;
    FileRevisionInfo.RevisionsListInfo info;

    private async System.Threading.Tasks.Task<FileRevisionInfo.RevisionsListInfo> UpdateInfo(System.Action<FileRevisionInfo.RevisionsListInfo> onComplete)
    {
        Debug.LogFormat("UpdateInfo");

        AuthController.CancelAuth();

        request = FileRevisionInfo.Get("1S8l6oYcy1V3cjE8UNiRKcXQuV3xdxXD4JPH_9czdDUg");
        request.Fields = new List<string> { "revisions" };
        Debug.LogFormat("UpdateInfo1 {0}", request);

        //Debug.LogFormat("UpdateInfo {0}", request.ResponseData, request.);
        var result = await request.Send();        

        onComplete(result);

        return result;
    }

    public static class FileRevisionInfo
    {
        public class RevisionInfo : UnityGoogleDrive.Data.ResourceData
        {
            public override string Kind => "drive#revision";
            public string Id { get; private set; }
            public System.DateTime ModifiedTime { get; private set; }
        }

        public class RevisionsListInfo : UnityGoogleDrive.Data.ResourceData
        {
            public override string Kind => "drive#revisionList";
            public RevisionInfo[] Revisions { get; private set; }
        }

        public class GetRequest : GoogleDriveRequest<RevisionsListInfo>
        {
            //1S8l6oYcy1V3cjE8UNiRKcXQuV3xdxXD4JPH_9czdDUg
            public GetRequest(string fileId)
                : base(@"https://www.googleapis.com/drive/v3/files/" + fileId + "/revisions", UnityWebRequest.kHttpVerbGET) { }
        }

        public static GetRequest Get(string fileId)
        {
            return new GetRequest(fileId);
        }
    }
}