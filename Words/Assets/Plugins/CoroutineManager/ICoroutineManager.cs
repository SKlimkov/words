﻿using UnityEngine;

namespace CoroutineHandling
{
    public interface ICoroutineManager
    {
        void ExecuteOnFixedUpdate(IFixedRoutinable routinable);
        void ExecuteOnEndOfFrame(IEndOfFrameRoutinable executable);
        void ExecuteOnUpdate(IFrameRoutinable routinable);

        void CallRoutinable(ELazyActionType actionType, IRoutinableSimple routinable);
        void LazyCall(System.Action action, float delay);
        void LazyCall(ELazyActionType actionType, System.Action action);

        void LazyCall<TParam>(System.Action<TParam> action, TParam param, float delay);

        void StartCoroutine(System.Collections.IEnumerator routine);
    }
}