﻿public class NormalWinddowAnimationState : IWindowAnimationState
{
    public void OnAnimationBreak(IWindowAnimationContext context)
    {
        return;
    }

    public EWindowAnimationState Type { get { return EWindowAnimationState.Normal; } }
}