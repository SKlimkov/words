﻿namespace CustomListener
{
    /// <summary>
    /// This interface must be implemented by all event parameter types
    /// </summary>
    public interface IEventParamBase
    {

    }
}