﻿using AssetsSystem;
using CoroutineHandling;
using GoogleSheetsToUnity;
using System;
using System.Collections;
using UniRx.Async;
using UnityEngine;
using Zenject;

namespace GoogleRemoteSync
{
    public interface ISyncManager
    {
        UniTask<SyncManager.Result> Sync(SyncManager.Config config);
    }

    public class SyncManager : ISyncManager
    {
        [Inject]
        private void ResolveDependencies(DiContainer container)
        {
            m_AssetManager = container.Resolve<IAssetManager>();
        }

        public async UniTask<Result> Sync(Config config)
        {
            try
            {
                var authorisationRefreshResult = await SpreadsheetAsynkManager.RefreshToken();

                if (authorisationRefreshResult.isSuccess)
                {
                    //Debug.LogFormat("Refreshing complete!");

                    return await SyncAll(config);
                }

                return new Result();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private IAssetManager m_AssetManager;

        private async UniTask<Result> SyncAll(Config config)
        {
            Debug.LogFormat("SyncAll");

            var isAssetForFile = m_AssetManager.CheckAssetForFile(config.fileId);

            FileAsset fileAsset;
            bool isNeedTosync = true;

            if (isAssetForFile)
            {
                fileAsset = await m_AssetManager.LoadAssetFile(config.fileId);

                if (fileAsset.LastModified.ToDateTime() >= config.lasRevision)
                {
                    isNeedTosync = false;
                }

                Debug.LogFormat("File: {0}", fileAsset);
            }
            else
            {
                fileAsset = new FileAsset(config.fileId, config.lasRevision);
            }

            m_AssetManager.CreateDecorator(fileAsset);

            bool isSuccess = true;

            try
            {
                if (isNeedTosync)
                {
                    var sheets = await SpreadsheetAsynkManager.EnumerateWorkSheets(new GSTU_Search(config.fileId, null));

                    if (sheets.isSuccess)
                    {
                        var decorator = m_AssetManager.GetDecoratorForFile(config.fileId);
                        decorator.InitializeSheets(sheets.sheets);

                        foreach (var entry in sheets.sheets)
                        {
                            var sheetInfo = await SpreadsheetAsynkManager.EditorRead(
                                new GSTU_Search(config.fileId, entry.title), entry);

                            //Debug.LogFormat("SyncAll {0}", sheetInfo.sheet);

                            if (sheetInfo.isSuccess)
                            {
                                decorator.InitializeWordSet(sheetInfo.sheet, entry);
                            }
                            else
                            {
                                isSuccess = false;
                                Debug.LogErrorFormat("Can't retrivie sheet with name {0}", entry.title);
                            }
                        }

                        //Debug.LogFormat("decorator {0}, {1}", decorator, config.lasRevision);

                        decorator.CompleteInitialization(config.lasRevision);
                    }
                    else
                    {
                        isSuccess = false;
                        Debug.LogErrorFormat("Can't retrivie sheetlist for file {0}", config.fileId);
                    }
                }
                else
                {
                    Debug.LogFormat("Words count: {0}", fileAsset.WordsCount);
                }

                if (!isAssetForFile || isNeedTosync)
                {
                    Debug.LogFormat("Need to save file {0}, {1}, {2}", fileAsset.FileId, fileAsset.LastModified, fileAsset.WordsSetList.Count);
                    m_AssetManager.RewriteFile(config.fileId, fileAsset);
                }

                return new Result() { isSuccess = isSuccess };
            }
            catch (Exception e)
            {
                throw e;
            }         
        }

        public struct Result
        {
            public bool isSuccess;
        }

        public struct Config
        {
            public string fileId;
            public DateTime lasRevision;
        }
    }    
}