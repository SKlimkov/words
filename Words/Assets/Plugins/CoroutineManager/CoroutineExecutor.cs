﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoroutineHandling
{
    public interface ICoroutineExecutor
    {
        void AddRoutinable(IRoutinableSimple routinable);
    }

    public class CoroutineExecutor : ICoroutineExecutor
    {
        public CoroutineExecutor(int type, IRoutineActionsHolder actionsHolder, MonoBehaviour monoInstance)
        {
            m_ActionType = (ELazyActionType)type;
            m_ActionsHolder = actionsHolder;
            m_MonoInstance = monoInstance;
        }

        private IRoutineActionsHolder m_ActionsHolder;
        private ELazyActionType m_ActionType;
        private MonoBehaviour m_MonoInstance;

        private HashSet<IRoutinableSimple> m_Routinables = new HashSet<IRoutinableSimple>();
        private HashSet<IRoutinableSimple> m_RoutinablesToAdd = new HashSet<IRoutinableSimple>();

        private bool m_IsStarted;
        public void AddRoutinable(IRoutinableSimple routinable)
        {
            m_RoutinablesToAdd.Add(routinable);
            if (!m_IsStarted)
            {
                m_MonoInstance.StartCoroutine(HandleRoutine());
            }
        }

        private IEnumerator HandleRoutine()
        {
            m_IsStarted = true;

            yield return m_ActionsHolder.GetAction(m_ActionType);

            foreach (var entry in m_RoutinablesToAdd)
            {
                m_Routinables.Add(entry);
            }

            m_RoutinablesToAdd.Clear();

            foreach (var entry in m_Routinables)
            {
                entry.OnAction();
            }

            m_Routinables.Clear();

            m_IsStarted = false;
        }
    }
}