﻿using UnityEngine;

namespace AssetsSystem
{
    public interface IAssetContent
    {
        void OnEnable(string assetName);
    }

    [System.Serializable]
    public abstract class DataAsset<TContent> : ScriptableObject
        where TContent : IAssetContent
    {
        [SerializeField]
        protected TContent content;

        protected virtual void OnEnable()
        {
            if (ContentInstance != null)
            {
                ContentInstance.OnEnable(this.name);
            }
        }

        public virtual TContent ContentInstance { get { return content; } }
    }
}