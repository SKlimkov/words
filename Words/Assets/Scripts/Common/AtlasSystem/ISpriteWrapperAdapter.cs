﻿using UnityEngine;

namespace AtlasSystem
{
    public interface ISpriteWrapperAdapter
    {
        void SetSprite(Component wrapper, Sprite sprite);
    }

    public interface ISpriteWrapperAdapter<TWrapper> : ISpriteWrapperAdapter
        where TWrapper : Component
    {
        void SetSprite(TWrapper wrapper, Sprite sprite);
    }
}