﻿namespace CoroutineHandling
{
    public interface IRoutinableSimple
    {
        void OnAction();
    }
}