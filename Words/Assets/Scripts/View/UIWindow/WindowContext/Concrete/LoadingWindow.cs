﻿using Zenject;

public class LoadingWindow : WindowContextBase<LoadingWindow.ShowData, LoadingWindow.HideData>
{
    #region INITIALIZATION

    [Inject]
    private void ResolveDependencies(DiContainer container) { }

    protected override void Awake()
    {
        base.Awake();
    }

    #endregion

    #region WINDOW

    protected override void OnHidePrepare(HideData data) { }

    protected override void OnShowPrepare(ShowData data) { }

    #endregion

    #region NESTED_TYPES

    public struct ShowData : IShowWindowData
    {
        public bool IsNeedToClear { get { return true; } }
    }

    public struct HideData : IHideWindowData
    {
        public bool IsNeedToClear { get { return true; } }
    }

    #endregion
}