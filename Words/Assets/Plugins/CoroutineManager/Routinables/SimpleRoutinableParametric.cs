﻿namespace CoroutineHandling
{
    public struct SimpleRoutinableParametric<TParam> : IRoutinableSimple
    {
        private System.Action<TParam> m_Action;
        private TParam m_Param;
        public SimpleRoutinableParametric(System.Action<TParam> action, TParam param)
        {
            m_Action = action;
            m_Param = param;
        }

        public void OnAction()
        {
            m_Action(m_Param);
        }
    }
}