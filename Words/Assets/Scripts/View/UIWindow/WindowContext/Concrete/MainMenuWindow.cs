﻿using CustomListener;
using CustomUi;
using GameSystems.EventSystem;
using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MainMenuWindow : WindowContextBase<MainMenuWindow.ShowData, MainMenuWindow.HideData>
{
    #region INITIALIZATION

    [Inject]
    private void ResolveDependencies(DiContainer container)
    {
        m_UiEventsFasade = container.Resolve<PublisherFacade<UiEventFacade.IEventParam>>();
        m_SystemEventFasade = container.Resolve<PublisherFacade<SystemEventFacade.IEventParam>>();
    }

    protected override void Awake()
    {
        base.Awake();

        var buttons = GetComponentsInChildren<Button>();

        foreach(var entry in buttons)
        {
            if (entry.gameObject.name == "PlayButton")
            {
                m_StartGameButton = entry;
            }

            if (entry.gameObject.name == "ContentOptionButton")
            {
                m_ContentSelectionButton = entry;
            }
        }

        InitializeButton(m_StartGameButton, OnStartButtonClick);
        InitializeButton(m_ContentSelectionButton, OnSelectButtonClick);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        DeinitializeButton(m_StartGameButton, OnStartButtonClick);
        DeinitializeButton(m_ContentSelectionButton, OnSelectButtonClick);
    }

    private void InitializeButton(Button button, UnityEngine.Events.UnityAction action)
    {
        if (button != null)
        {
            button.onClick.AddListener(action);
        }
        else
        {
            throw new NullReferenceException(string.Format("Button is null!"));
        }
    }

    private void DeinitializeButton(Button button, UnityEngine.Events.UnityAction action)
    {
        if (button != null)
        {
            button.onClick.RemoveListener(action);
        }
        else
        {
            throw new NullReferenceException(string.Format("Button is null!"));
        }
    }

    #endregion

    #region CUSTOM

    private Button m_StartGameButton;
    private Button m_ContentSelectionButton;

    private PublisherFacade<UiEventFacade.IEventParam> m_UiEventsFasade;
    private PublisherFacade<SystemEventFacade.IEventParam> m_SystemEventFasade;

    private void OnStartButtonClick()
    {
        Debug.LogFormat("OnStartButtonClick");

        m_SystemEventFasade.TriggerEvent(new SystemEventFacade.OnGameSessionNeedToStart.Param());

        m_UiEventsFasade.TriggerEvent(new UiEventFacade.ShowWindowEvent.Param("GameWindow", new GameViewWindow.ShowData()));
        m_UiEventsFasade.TriggerEvent(new UiEventFacade.HideWindowEvent.Param("MainMenuWindow", new MainMenuWindow.HideData()));
    }

    private void OnSelectButtonClick()
    {
        Debug.LogFormat("OnSelectButtonClick");

        m_UiEventsFasade.TriggerEvent(new UiEventFacade.ShowWindowEvent.Param("ContentOptionsWindow", new ContentOptionsWindow.ShowData()));
        m_UiEventsFasade.TriggerEvent(new UiEventFacade.HideWindowEvent.Param("MainMenuWindow", new MainMenuWindow.HideData()));
    }

    #endregion

    #region WINDOW

    protected override void OnHidePrepare(HideData data) { }

    protected override void OnShowPrepare(ShowData data) { }

    #endregion

    #region NESTED_TYPES

    public struct ShowData : IShowWindowData
    {
        public bool IsNeedToClear { get { return true; } }
    }

    public struct HideData : IHideWindowData
    {
        public bool IsNeedToClear { get { return true; } }
    }

    #endregion
}