﻿using System;

namespace CustomFactory
{
    public class Factory<TConfig, TResult> : IFactory<TConfig, TResult>
        where TResult : IConfigurateable<TConfig>
    {
        public virtual TResult Create(TConfig config)
        {
            var result = CreateInstance();
            result.Configurate(config);
            return result;
        }

        protected virtual TResult CreateInstance()
        {
            return (TResult)Activator.CreateInstance(typeof(TResult));
        }
    }
}