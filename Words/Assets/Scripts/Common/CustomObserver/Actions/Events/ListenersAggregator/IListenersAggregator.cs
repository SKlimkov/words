﻿namespace CustomListener
{
    public interface IListenersAggregator : IHashCodeGetable
    {
        void AddListener<TParam>(OnEventTriggerHandler<TParam> listener) where TParam : IEventParamBase;
        void RemoveListener<TParam>(OnEventTriggerHandler<TParam> listener) where TParam : IEventParamBase;
        void ClearAllListeners();
    }
}