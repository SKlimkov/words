﻿using AssetsSystem.StorageSystem;
using GoogleSheetsToUnity;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AssetsSystem
{
    public interface IFileAssetDecorator
    {
        IStorage<Properties> Sheets { get; }
        AssetStorage<WordsSetAsset> AssetStorage { get; }
        void InitializeSheets(List<Properties> sheetList);
        void InitializeWordSet(GstuSpreadSheet sheet, Properties sheetProperty);
        void CompleteInitialization(DateTime lasRevision);

        bool IsEmpty { get; }
    }

    public class FileAssetDecorator : IFileAssetDecorator
    {
        public class Storage : Storage<Properties> { }

        public FileAssetDecorator(FileAsset asset = null)
        {
            SheetStorage = new Storage();
            AssetStorage = new AssetStorage<WordsSetAsset>();

            //Debug.LogFormat("!!!!Create new FileAssetDecorator! {0}, {1}, {2}", AssetStorage.Count, asset.WordsSetList, asset.WordsSetList.Count);
            if (asset != null)
            {
                FileAssetinstance = asset;
                foreach (var entry in asset.WordsSetList)
                {
                    Debug.LogFormat("{0}, {1}", entry.Name, entry.IsChoosed);
                    AssetStorage.Add(entry);
                }
                IsEmpty = true;
            }
        }

        public bool IsEmpty { get; private set; }

        private Storage<Properties> SheetStorage { get; set; }
        public IStorage<Properties> Sheets { get { return SheetStorage; } }
        public AssetStorage<WordsSetAsset> AssetStorage { get; private set; }
        public FileAsset FileAssetinstance { get; }

        public void InitializeSheets(List<Properties> sheetList)
        {
            SheetStorage.Initialize(sheetList);
        }

        public void InitializeWordSet(GstuSpreadSheet sheet, Properties sheetProperty)
        {
            Debug.LogFormat("InitializeWordSet {0}, {1}", sheetProperty.sheetId, sheetProperty.title);
            var word = GetAsset();
            Synchronize(sheet, word, sheetProperty);
            AssetStorage.Add(word);
        }

        private static void Synchronize<TObject>(GstuSpreadSheet sheet, TObject asset, Properties sheetProperty)
            where TObject : ISynchronizeableAsset
        {
            if (asset == null)
            {
                throw new System.Exception("Assets is null!");
            }

            asset.Synchronize(sheet, sheetProperty);
        }

        private WordsSetAsset GetAsset()
        {
            return new WordsSetAsset();
        }

        public void CompleteInitialization(DateTime lasRevision)
        {
            var wordSetList = AssetStorage.Enumerate();

            //Debug.LogFormat("decorator {0}, {1}, {2}", AssetStorage.Get(0).Words.Count, AssetStorage.Count, AssetStorage.Get(0).GetHashCode());
            foreach (var entry in wordSetList)
            {
                FileAssetinstance.AddWordSet(entry);
            }
            FileAssetinstance.Initialize(lasRevision);
        }
    }
}