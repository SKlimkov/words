﻿namespace DelegatesHandling
{
    public interface IEventExecutor<THandler>
    {
        void Execute();
        //THandler Event { get; }
    }
}