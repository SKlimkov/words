﻿namespace CustomListener
{
    /// <summary>
    /// Interface for object that will store event id
    /// </summary>
    public interface IEventMember
    {
        /// <summary>
        /// Property to store event id signature. Event id assign in EventsAggregator static constructor automatically.
        /// </summary>
        int EventId { get; }
    }
}