﻿using CustomFactory;
using UnityEngine;

namespace ZenjectServices
{
    public abstract class MonoConfigurateable<TMonoConfig> : MonoBehaviour, IConfigurateable<TMonoConfig>
        where TMonoConfig : IMonoConfigWrapper
    {
        public abstract void Configurate(TMonoConfig config);
    }
}