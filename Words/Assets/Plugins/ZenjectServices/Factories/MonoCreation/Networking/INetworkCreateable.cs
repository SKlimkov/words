﻿using CustomFactory;
using ZenjectServices;

public interface INetworkCreateable<TMonoConfig> : IConfigurateable<TMonoConfig>
    where TMonoConfig : IMonoConfigWrapper
{

}