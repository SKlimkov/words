﻿using CustomFactory;
using UnityEngine;
using Zenject;

namespace ZenjectServices
{
    public abstract class PlaceHolderFactoryWrapper<TConfig, TResult> : PlaceHolderFactory<TConfig, TResult>
        where TResult : IConfigurateable<TConfig>
    {
        public PlaceHolderFactoryWrapper(DiContainer container) : base(container) { }

        public override TResult Create(TConfig config)
        {
            Container.FlushBindings();
            var result = CreateNewInstance(config);
            result.Configurate(config);
            return result;
        }

        protected abstract TResult CreateNewInstance(TConfig config);

        public override TResult Create(TConfig config, GameObject prefab)
        {
            throw new System.Exception(string.Format("You try to use factory {0} as monofactory, but it is prohibited!", this.GetType()));
        }
    }
}