﻿using AssetsSystem;
using System;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Networking;
using UnityGoogleDrive;

namespace GoogleRemoteSync
{
    public static class FileRevisionInfo
    {
        public class RevisionInfo : UnityGoogleDrive.Data.ResourceData
        {
            public override string Kind => "drive#revision";
            public string Id { get; private set; }
            public DateTime ModifiedTime { get; private set; }
        }

        public class RevisionsListInfo : UnityGoogleDrive.Data.ResourceData
        {
            public override string Kind => "drive#revisionList";
            public RevisionInfo[] Revisions { get; private set; }
        }

        public class GetRequest : GoogleDriveRequest<RevisionsListInfo>
        {
            public GetRequest(string fileId)
                : base(@"https://www.googleapis.com/drive/v3/files/" + fileId + "/revisions", UnityWebRequest.kHttpVerbGET) { }
        }

        public static GetRequest Get(string fileId)
        {
            return new GetRequest(fileId);
        }
    }

    public interface IRevisionVerifyingManager
    {
        UniTask<RevisionVerifyingManager.Result> Verify(RevisionVerifyingManager.Config config);
    }

    public class RevisionVerifyingManager : IRevisionVerifyingManager
    {
        public async UniTask<Result> Verify(Config config)
        {
            AuthController.CancelAuth();

            var request = FileRevisionInfo.Get(config.sheetId);
            request.Fields = new List<string> { "revisions" };
            //Debug.LogFormat("CheckRevisionActuality {0}", request);
            try
            {
                var info = await request.Send();
                var result = new Result() { request = request, info = info };
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public struct Result
        {
            public FileRevisionInfo.GetRequest request;
            public FileRevisionInfo.RevisionsListInfo info;
        }

        public struct Config
        {
            public string sheetId;
        }        
    }
}