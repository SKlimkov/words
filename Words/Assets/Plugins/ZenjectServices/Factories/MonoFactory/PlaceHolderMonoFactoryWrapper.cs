﻿using CustomFactory;
using System;
using UnityEngine;
using Zenject;
using Zenject.Internal;

namespace ZenjectServices
{
    public interface IInjectable
    {
        void Inject(DiContainer container);
    }

    [Obsolete]
    public class PlaceHolderMonoFactoryWrapper<TConfig, TResult> : PlaceHolderFactory<TConfig, TResult>
        where TResult : IConfigurateable<TConfig>, IInjectable
    {
        public PlaceHolderMonoFactoryWrapper(DiContainer container) : base(container) { }

        public override TResult Create(TConfig config)
        {
            throw new System.Exception(string.Format("You try to use monofactory {0} as factory, but it is prohibited!", this.GetType()));
        }

        public override TResult Create(TConfig config, GameObject prefab)
        {
            var result = CreateInternal(prefab);
            if (result == null)
            {
                Debug.LogErrorFormat("PlaceHolderMonoFactoryWrapper {0}, {1}, {2}", prefab, typeof(TConfig), typeof(TResult));
            }
            result.Configurate(config);

            return result;
        }

        protected virtual TResult CreateInternal(GameObject prefab)
        {
            bool shouldMakeActive;
            var goInstance = InstantiatePrefab(prefab, out shouldMakeActive);

            InjectGameObject(goInstance);

            if (shouldMakeActive)
            {
                goInstance.SetActiveSafe(true);
            }

            var result = goInstance.GetComponent<TResult>();

            return result;
        }

        protected GameObject InstantiatePrefab(GameObject prefab, out bool shouldMakeActive)
        {
            if (prefab == null)
            {
                throw new System.Exception("Prefab is null!");
            }

            var wasActive = prefab.activeSelf;

            if (wasActive)
            {
                prefab.SetActiveSafe(false);
            }

            shouldMakeActive = wasActive;

            try
            {
                var result = GameObject.Instantiate(prefab) as GameObject;
                //Debug.LogFormat("try {0}", prefab.activeSelf);
                return result;
            }
            finally
            {
                if (wasActive)
                {                    
                    // Always make sure to reset prefab state otherwise this change could be saved
                    // persistently
                    prefab.SetActiveSafe(true);

                    //Debug.LogFormat("finally {0}", prefab.activeSelf);
                }
            }
        }

        public void InjectGameObject(GameObject gameObject)
        {
            Container.FlushBindings();

            var children = gameObject.GetComponentsInChildren<MonoBehaviour>(true);

            /*if (typeof(TResult) == typeof(SlotViewBase))
            {
                Debug.LogErrorFormat("{0}", children.Length);
            }*/            

            foreach(var entry in children)
            {
                /*if (typeof(TResult) == typeof(SlotViewBase) && (entry.GetType() == typeof(DropTargetBase) || entry.GetType() == typeof(DropTargetBattle)))
                    Debug.LogFormat("{0}, {1}", entry.gameObject.name, entry.GetType());*/

                if (entry is IInjectable)
                {
                    (entry as IInjectable).Inject(Container);
                }
            }
        }
    }
}
