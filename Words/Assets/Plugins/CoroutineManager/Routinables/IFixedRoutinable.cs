﻿namespace CoroutineHandling
{
    public interface IFixedRoutinable : IRoutinable
    {
        bool GetConditionCheck();
        void OnFinish();
    }
}