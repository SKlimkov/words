﻿//using ObjectPoolCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Zenject;
using ZenjectServices;

public class ReflectionHelper : MonoBehaviour
{
    public static IEnumerable<Type> EnumeratePossibleToAssignTo<T>(T type) where T : Type
    {
        List<Type> objects = new List<Type>();

        var types = Assembly.GetAssembly(type).GetTypes().Where(entry => 
        !entry.IsInterface && 
        !entry.IsAbstract && 
        (type.IsAssignableFrom(entry) || entry.IsSubclassOf(type)));

        foreach (Type entry in types)
        {
            objects.Add(entry);
        }

        return objects;
    }

    private static Dictionary<Type, Type[]> m_GenericArgumentsCache = new Dictionary<Type, Type[]>();

    public static Type[] GetGenericArgument(Type type)
    {
        if (m_GenericArgumentsCache.ContainsKey(type))
        {
            return m_GenericArgumentsCache[type];
        }
        else
        {
            var arguments = type.GetGenericArguments();

            if (!m_GenericArgumentsCache.ContainsKey(type))
            {
                m_GenericArgumentsCache.Add(type, arguments);
            }

            return arguments;
        }
    }

    #region ZENJECT_HELPERS

    public static Type GetFactoryInterfaceForBinding(Type configType, Type instanceType)
    {
        return typeof(IPlaceHolderFactoryWrapper<,>).MakeGenericType(configType, instanceType);
    }

    public static Type GetFactoryTypeForBinding(Type configType, Type instanceType)
    {
        return typeof(PlaceHolderFactoryWrapper<,>).MakeGenericType(configType, instanceType);
    }

    public static void BindFactoryByReflection(DiContainer container, Type configType, Type instanceType, Type factoryClassType = null)
    {
        var interfaceType = GetFactoryInterfaceForBinding(configType, instanceType);
        Type classType;
        if (factoryClassType == null)
        {
            classType = GetFactoryTypeForBinding(configType, instanceType);
        }
        else
        {
            classType = factoryClassType;
        }
        container.Bind(interfaceType).To(classType).AsSingle();
        GetGenericArgument(classType);
    }    

    /*public static Type GetPoolTypeForBinding(Type configType, Type instanceType)
    {
        return typeof(SingleStackObjectPool<,>).MakeGenericType(configType, instanceType);
    }*/

    /*public static void BindSingleStackObjectPoolByReflection(DiContainer container, Type configType, Type instanceType)
    {        
        var poolType = GetPoolTypeForBinding(configType, instanceType);
        container.Bind(poolType).ToSelf().AsSingle();
        GetGenericArgument(poolType);
    }

    public static object ResolveSingleStackObjectPoolByReflection(DiContainer container, Type configType, Type instanceType)
    {
        var poolType = GetPoolTypeForBinding(configType, instanceType);
        return container.Resolve(poolType);
    }*/

    #endregion
}