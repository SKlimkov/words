﻿using System.Collections.Generic;
using UnityEngine;

namespace AssetsSystem.StorageSystem
{
    public interface IDictionaryStore<TKey, TStorable> : IStorage<TStorable>
    {
        TStorable Get(TKey key);
    }

    public class AssetStorage<TStorable> : IDictionaryStore<int, TStorable>
        where TStorable : IUniqueObject
    {
        Dictionary<int, TStorable> dict = new Dictionary<int, TStorable>();

        public IEnumerable<TStorable> Enumerate()
        {
            foreach (var entry in dict)
            {
                yield return entry.Value;
            }
        }

        public void Add(TStorable storable)
        {
            var key = storable.Id;
            if (!dict.ContainsKey(key))
            {
                dict.Add(key, storable);
            }
            else
            {
                dict[key] = storable;
                Debug.LogFormat("Add {0}", key);
            }

            //Debug.LogFormat("Add {0}", dict.Count);
        }

        public TStorable Get(int key)
        {
            TStorable result;

            var requestResult = dict.TryGetValue(key, out result);

            if (requestResult)
            {
                return result;
            }
            else
            {
                throw new System.Exception(string.Format("Can't find value for {0} key in dict", key));
            }
        }

        public int Count { get { return dict.Count; } }
    }
}