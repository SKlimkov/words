﻿namespace DelegatesHandling
{
    public interface IMessage
    {

    }

    public interface IEventMessage : IMessage
    {
        bool IsNeedToClear { get; }
    }
}