﻿namespace ZenjectServices
{
    public interface IMonoConfigWrapper
    {
        MonoConfig MonoConfig { get; }
    }
}