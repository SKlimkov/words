﻿namespace CustomActions
{
    public interface IResultableGenericExecutable<TParam, TResult>
        where TParam : IExecutableParam
        where TResult : IExecutableResult
    {
        TResult Execute(TParam param);
    }
}