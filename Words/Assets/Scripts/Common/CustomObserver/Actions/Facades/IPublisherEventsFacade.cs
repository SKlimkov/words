﻿namespace CustomListener
{
    /// <summary>
    /// Interface provides events and callbacks functionality for users.
    /// Users can only listen context events, and only execute context callbacks.
    /// All delegates executed on context or internal (non-user) side woul'd be called as events.
    /// All delegates executed on user side woul'd be called as callbacks.
    /// </summary>
    public interface IPublisherEventsFacade
    {
        #region CONTEXT_EVENTS

        /// <summary>
        /// Generic method signature for adding listeners to context event by external users.
        /// </summary>
        /// <typeparam name="TParam">Generic param for event argument. Used for define for wich event the listener must be added</typeparam>
        /// <param name="listener">Addable delegate that woul'd be added as event listener.</param>
        void AddEventListener<TParam>(OnEventTriggerHandler<TParam> listener) where TParam : IEventParamBase;

        /// <summary>
        /// Generic method signature for removing listeners to context event by external users.
        /// </summary>
        /// <typeparam name="TParam">Generic param for event argument. Used for define from wich event the listener must be removed.</typeparam>
        /// <param name="listener">Removable delegate that woul'd be removed as event listener.</param>
        void RemoveEventListener<TParam>(OnEventTriggerHandler<TParam> listener) where TParam : IEventParamBase;

        #endregion        

        void ClearAllListeners();
    }
}