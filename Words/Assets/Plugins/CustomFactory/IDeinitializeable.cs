﻿namespace CustomFactory
{
    public interface IDeinitializeable
    {
        void Deinitialize();
    }
}
