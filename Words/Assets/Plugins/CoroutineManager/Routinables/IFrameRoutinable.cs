﻿namespace CoroutineHandling
{
    public interface IFrameRoutinable : IRoutinable
    {
        bool GetConditionCheck();
        void OnFinish();
    }
}