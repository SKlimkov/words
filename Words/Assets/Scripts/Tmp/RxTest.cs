﻿using System;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Networking;

public class RxTest : MonoBehaviour
{
    void Start()
    {
        var uri = "http://www.nixsolutions.com/";
        var observer = Observer.Create<string>(
            result => Debug.LogFormat("OnNext {0}", result),
            exception => Debug.LogFormat("OnError: {0}", exception.Message),
            () => Debug.Log("OnCompleted"));

        //ObservableWWW.Get(uri).Subscribe(observer);

        var observable = GetTextAsync(uri).ToObservable();
        observable.Subscribe(observer);        
        Get(observable);
    }

    async void Get(IObservable<string> observable)
    {
        try
        {
            var result = await observable;
            Debug.Log(result);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }        
    }
    
    private IObservable<string> GetTextObservable(string uri)
    {
        return GetTextAsync(uri).ToObservable();
    }

    private async UniTask<string> GetTextAsync(string uri)
    {
        var uwr = UnityWebRequest.Get(uri);

        // SendWebRequest가 끝날 때까지 await 
        await uwr.SendWebRequest();

        if (uwr.isHttpError || uwr.isNetworkError)
        {
            // 실패시 예외 throw
            throw new Exception(uwr.error);
        }

        return uwr.downloadHandler.text;
    }
}