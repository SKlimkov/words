﻿using System;
using CustomFactory;
using DelegatesHandling;
using UnityEngine;
using Zenject;
using ZenjectServices;

public abstract class WindowContextBase : MonoConfigurateable<WindowContextBase.Config>, IWindowContext, IInjectable
{
    protected DiContainer container;

    #region INJECTABLE

    public virtual void Inject(DiContainer container) {  }

    #endregion

    public override void Configurate(Config config)
    {
        container = config.Container;
    }

    public abstract void Show(IShowWindowData data);
    public abstract void Hide(IHideWindowData data);

    protected abstract void Lock();

    protected abstract void Unlock();

    public abstract string WindowId { get; }

    #region NESTED_CLASSES   

    public struct Config : IMonoConfigWrapper
    {
        public DiContainer Container;
        public MonoConfig MonoConfig { get; private set; }

        public Config(DiContainer container, MonoConfig transformConfig)
        {
            Container = container;
            MonoConfig = new MonoConfig(transformConfig);
        }
    }

    public class Factory : MonoFactory<Config, WindowContextBase>
    {
        public Factory(DiContainer container) : base(container) { }
    }

    #endregion
}

[RequireComponent(typeof(IWindowView))]
public abstract class WindowContextBase<TShowData, THideData> : WindowContextBase, IWindowAnimationContext, IInjectable
    where TShowData : IShowWindowData
    where THideData : IHideWindowData
{
    #region INITIALIZATION

    [Inject]
    private void ResolveDependencies(DiContainer container)
    {
        m_WindowManager = container.Resolve<IWindowManager>();

        var showHolder = new TypedMessagedVoidEventHolder<TShowData>();
        m_ShowHolder = showHolder;
        m_ShowExecutor = showHolder;

        var hideHolder = new TypedMessagedVoidEventHolder<THideData>();
        m_HideHolder = hideHolder;
        m_HideExecutor = hideHolder;
    }

    protected virtual void Awake()
    {
        m_WindowView = GetComponent<IWindowView>();
        m_WindowManager.RegisterWindow(WindowId, this);

        m_NormalState = new NormalWinddowAnimationState();
        m_ShownigState = new ShowingWindowAnimationState();
        m_HidingState = new HidingWindowAnimationState();

        m_CurrentState = m_NormalState;
    }    

    protected virtual void OnDestroy()
    {
        m_WindowManager.UnRegisterWindow(WindowId);
    }

    #endregion

    #region WINDOW

    [SerializeField]
    private string m_WindowId;
    private IWindowView m_WindowView;
    private IWindowManager m_WindowManager;

    private IWindowAnimationState m_NormalState;
    private IWindowAnimationState m_ShownigState;
    private IWindowAnimationState m_HidingState;

    private IWindowAnimationState m_CurrentState;

    protected override void Lock()
    {
        m_WindowView.Lock();
    }

    protected override void Unlock()
    {
        m_WindowView.Unlock();
    }

    public override void Show(IShowWindowData data)
    {
        if (m_CurrentState.Type != EWindowAnimationState.Showing)
        {
            m_CurrentState.OnAnimationBreak(this);
        }

        m_CurrentState = m_ShownigState;

        if (data is TShowData)
        {
            var typedData = (TShowData)data;
            OnShowPrepare(typedData);
            m_WindowView.Show();
            OnShowStarted(m_WindowView);
        }
        else
        {
            Debug.LogErrorFormat("Wrong type of message. Its should be {0}, but it is {1}", typeof(TShowData), data.GetType());
        }
    }

    public override void Hide(IHideWindowData data)
    {
        if (m_CurrentState.Type != EWindowAnimationState.Hiding)
        {
            m_CurrentState.OnAnimationBreak(this);
        }

        m_CurrentState = m_HidingState;

        if (data is THideData)
        {
            var typedData = (THideData)data;
            OnHidePrepare(typedData);
            m_WindowView.Hide();
            OnHideStarted(m_WindowView);
        }
        else
        {
            Debug.LogErrorFormat("Wrong type of message. Its should be {0}, but it is {1}", typeof(THideData), data.GetType());
        }
    }

    protected abstract void OnShowPrepare(TShowData data);
    protected abstract void OnHidePrepare(THideData data);

    public override string WindowId { get { return m_WindowId; } }

    #endregion

    #region ANIMATION_CONTEXT

    public void OnShowingAnimationAborted()
    {
        m_ShowHolder.ClearListeners();
    }

    public void OnHidingAnimationAborted()
    {
        m_HideHolder.ClearListeners();
    }

    #endregion

    #region EVENTS

    public IMessagedEventHolder<TypedVoidDelegate<TShowData>, TShowData> OnShowAnimationComplete { get { return m_ShowHolder; } }

    private IMessagedEventHolder<TypedVoidDelegate<TShowData>, TShowData> m_ShowHolder;
    private IMessagedEventExecutor<TShowData> m_ShowExecutor;   
    
    private void OnShowStarted(IWindowView window)
    {
        window.OnShowingUp.AddListener(OnShowComplete);
    }

    private void OnShowComplete()
    {
        //m_IsAnimated = false;
        m_CurrentState = m_NormalState;
        m_ShowExecutor.Execute();
    }

    public IMessagedEventHolder<TypedVoidDelegate<THideData>, THideData> OnHideAnimationComplete { get { return m_HideHolder; } }

    private IMessagedEventHolder<TypedVoidDelegate<THideData>, THideData> m_HideHolder;
    private IMessagedEventExecutor<THideData> m_HideExecutor;

    private void OnHideStarted(IWindowView window)
    {
        window.OnHidingUp.AddListener(OnHideComplete);
    }

    private void OnHideComplete()
    {
        //m_IsAnimated = false;
        m_CurrentState = m_NormalState;
        m_HideExecutor.Execute();
    }

    #endregion    
}