﻿using CustomListener;

namespace CustomUi
{
    public class UiEventFacade : PublisherFacade<UiEventFacade.IEventParam>
    {
        #region EVENTS

        public interface IEventParam : IEventParamBase { }

        public sealed class ShowWindowEvent : EventBase<ShowWindowEvent.Param>
        {
            public ShowWindowEvent(IEventIdResolver eventIdResolver) : base(eventIdResolver) { }

            public struct Param : IEventParam
            {
                public Param(string windowId, IShowWindowData data)
                {
                    WindowId = windowId;
                    Data = data;
                }
                public string WindowId { get; private set; }
                public IShowWindowData Data { get; private set; }
            }

            public override bool IsNeedToUnlistenOnTrigger { get { return false; } }

            public class Factory : IEventFactory<ShowWindowEvent>
            {
                public EventBase CreateInstance(IEventIdResolver idResolver)
                {
                    return new ShowWindowEvent(idResolver);
                }
            }
        }

        public sealed class HideWindowEvent : EventBase<HideWindowEvent.Param>
        {
            public HideWindowEvent(IEventIdResolver eventIdResolver) : base(eventIdResolver) { }

            public struct Param : IEventParam
            {
                public Param(string windowId, IHideWindowData data)
                {
                    WindowId = windowId;
                    Data = data;
                }
                public string WindowId { get; private set; }
                public IHideWindowData Data { get; private set; }
            }

            public override bool IsNeedToUnlistenOnTrigger { get { return false; } }

            public class Factory : IEventFactory<HideWindowEvent>
            {
                public EventBase CreateInstance(IEventIdResolver idResolver)
                {
                    return new HideWindowEvent(idResolver);
                }
            }
        }

        #endregion
    }
}