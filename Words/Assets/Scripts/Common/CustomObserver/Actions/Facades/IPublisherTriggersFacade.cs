﻿namespace CustomListener
{
    /// <summary>
    /// Interface prvides events and callbacks functionality for external (non-user) context users.
    /// External users can only execute context events and only listen context callbacks
    /// </summary>
    public interface IPublisherTriggersFacade
    {
        #region CONTEXT_EVENTS

        /// <summary>
        /// Generic method signature for event executing (triggerting) by external users.
        /// </summary>
        /// <typeparam name="TType">Generic param for event argument. Used for define from wich event the listener must be executed.</typeparam>
        /// <param name="param">Param instance with execution data.</param>
        void TriggerEvent<TType>(TType param) where TType : IEventParamBase;

        #endregion        
    }
}