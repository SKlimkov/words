﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public static class ReflectionExtensions
{
    public static bool IsChildOf<T>(this T recentType, Type baseType)
        where T : Type
    {
        bool isAssignable = baseType.IsAssignableFrom(recentType);
        bool isSubclass = recentType.IsSubclassOf(baseType);
        return (isAssignable || isSubclass);
    }

    public static Type GetNestedType(Type targetType, Type searchedType, string searchedTypeName = null)
    {
        var nestedTypes = targetType.GetNestedTypes();

        Type result = null;

        foreach (var nested in nestedTypes)
        {
            if (IsName(nested.Name, searchedTypeName) && nested.IsChildOf(searchedType))
            {
                result = nested;
                return result;
            }
        }

        throw new Exception(string.Format("Can't find nested type for name {0} and type {1}!", searchedTypeName, searchedType));
    }

    private static bool IsName(string recentName, string targetName)
    {
        if (recentName == null)
        {
            return true;
        }
        else
        {
            return recentName == targetName;
        }
    }

    public static IEnumerable<Type> EnumerateNonAbstractInheritances<T>(this T type) where T : Type
    {
        List<Type> objects = new List<Type>();

        var types = Assembly.GetAssembly(type).GetTypes().Where(entry => !entry.IsInterface && !entry.IsAbstract && ((type).IsAssignableFrom(entry) || entry.IsSubclassOf(type)));

        foreach (Type entry in types)
        {
            objects.Add(entry);
        }

        return objects;
    }

    public static bool IsSubclassOfRawGeneric(this Type toCheck, Type generic)
    {
        while (toCheck != null && toCheck != typeof(object))
        {
            var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
            if (generic == cur)
            {
                return true;
            }
            toCheck = toCheck.BaseType;
        }
        return false;
    }
}