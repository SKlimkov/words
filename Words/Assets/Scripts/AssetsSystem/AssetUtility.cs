﻿using GoogleRemoteSync;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AssetsSystem
{
    public class AssetUtility : MonoBehaviour
    {
        private static SheetSettings m_SettingAsset;
        public static SheetSettings SettingAsset
        {
            get
            {
                if (m_SettingAsset == null)
                {
                    m_SettingAsset = Resources.Load<SheetSettings>("RemoteSyncSettings/SheetSettings");
                }

                return m_SettingAsset;
            }
        }

        /*public static TAsset GetAsset<TAsset>(string name, string pathPrefix, string pathPostfix)
        {
            var path = string.Format("{0}/{1}{2}", pathPrefix, name, pathPostfix);
            if (File.Exists(path))
            {

            }
        }*/

        public static List<TAsset> GetAssets<TAsset>(List<string> names, string pathPrefix, string pathPostfix)
            where TAsset : ScriptableObject
        {
            var result = new List<TAsset>();

            foreach (var entry in names)
            {
                var path = string.Format("{0}/{1}{2}", pathPrefix, entry, pathPostfix);
                var asset = Resources.Load<TAsset>(path);
                result.Add(asset);
            }

            return result;
        }        
    }
}