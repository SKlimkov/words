﻿using CustomFactory;
using UnityEngine;

namespace ZenjectServices
{
    public interface IPlaceHolderFactoryWrapper<TConfig, TResult>
        where TResult : IConfigurateable<TConfig>
    {
        TResult Create(TConfig config);
        TResult Create(TConfig config, GameObject prefab);
    }
}