﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Zenject;

namespace CoroutineHandling
{
    public class CoroutineManager : ICoroutineManager
    {
        private IRoutineActionsHolder m_RoutineHolder;

        private ICoroutineService m_CoroutineService;

        private Dictionary<int, ICoroutineExecutor> m_Executors;

        [Inject]
        private void ResolveDepenedencies(DiContainer container)
        {
            m_CoroutineService = container.ResolveId<ICoroutineService>("CoroutineService");
            m_RoutineHolder = new RoutineActionsHolder();

            m_Executors = new Dictionary<int, ICoroutineExecutor>();

            foreach (var entry in EnumHelper.GetValuesInt(typeof(ELazyActionType)))
            {
                m_Executors.Add(entry, new CoroutineExecutor(entry, m_RoutineHolder, m_CoroutineService.GetMonoInstance((ELazyActionType)entry)));
            }
        }

        #region LAZY_CALL

        private IEnumerator LazyVoidCall(System.Action action, float delay)
        {
            yield return m_RoutineHolder.GetAction(delay);
            action();
        }

        private IEnumerator LazyVoidCall<TParam>(System.Action<TParam> action, TParam param, float delay)
        {
            yield return m_RoutineHolder.GetAction(delay);
            action(param);
        }

        public void LazyCall(System.Action action, float delay)
        {
            m_CoroutineService.MonoInstance.StartCoroutine(LazyVoidCall(action, delay));
        }

        public void LazyCall<TParam>(System.Action<TParam> action, TParam param, float delay)
        {
            m_CoroutineService.MonoInstance.StartCoroutine(LazyVoidCall(action, param, delay));
        }

        public void CallRoutinable(ELazyActionType actionType, IRoutinableSimple routinable)
        {
            m_Executors[(int)actionType].AddRoutinable(routinable);
        }

        public void StartCoroutine(IEnumerator routine)
        {
            m_CoroutineService.MonoInstance.StartCoroutine(routine);
        }

        private IEnumerator LazyVoidCall(ELazyActionType actionType, System.Action action)
        {
            yield return m_RoutineHolder.GetAction(actionType);
            action();
        }

        public void LazyCall(ELazyActionType actionType, System.Action action)
        {
            m_CoroutineService.MonoInstance.StartCoroutine(LazyVoidCall(actionType, action));
        }

        #endregion

        #region UPDATE

        public void ExecuteOnUpdate(IFrameRoutinable routinable)
        {
            m_CoroutineService.MonoInstance.StartCoroutine(ExecuteOnUpdateInternal(routinable));
        }

        private System.Collections.IEnumerator ExecuteOnUpdateInternal(IFrameRoutinable routinable)
        {
            routinable.OnRoutinableStart();

            while (routinable.GetConditionCheck())
            {
                routinable.OnAction();
                yield return null;
            }

            routinable.OnFinish();
        }

        #endregion

        #region FIXED_UPDATE

        public void ExecuteOnFixedUpdate(IFixedRoutinable routinable)
        {
            m_CoroutineService.MonoInstance.StartCoroutine(ExecuteOnFixedUpdateInternal(routinable));
        }

        private System.Collections.IEnumerator ExecuteOnFixedUpdateInternal(IFixedRoutinable routinable)
        {
            routinable.OnRoutinableStart();

            while (routinable.GetConditionCheck())
            {
                routinable.OnAction();
                yield return new WaitForFixedUpdate();
            }

            routinable.OnFinish();
        }

        #endregion

        #region END_OF_FRAME

        public void ExecuteOnEndOfFrame(IEndOfFrameRoutinable executable)
        {
            m_CoroutineService.MonoInstance.StartCoroutine(ExecuteOnEndOfFrameInternal(executable));
        }

        private System.Collections.IEnumerator ExecuteOnEndOfFrameInternal(IEndOfFrameRoutinable executable)
        {
            executable.OnRoutinableStart();
            yield return new WaitForEndOfFrame();
            executable.OnAction();
        }

        #endregion

        #region HELPER

        private static class EnumHelper
        {
            private static Dictionary<Type, int[]> m_CachedEnumInt;

            static EnumHelper()
            {
                m_CachedEnumInt = new Dictionary<Type, int[]>();

                var types = Assembly.GetExecutingAssembly().GetTypes();
                foreach (var entry in types)
                {
                    if (entry.IsEnum)
                    {
                        m_CachedEnumInt.Add(entry, Enum.GetValues(entry) as int[]);
                    }
                }
            }

            public static void Init()
            {

            }

            public static int[] GetValuesInt(Type type)
            {
                return m_CachedEnumInt[type];
            }
        }

        #endregion
    }
}