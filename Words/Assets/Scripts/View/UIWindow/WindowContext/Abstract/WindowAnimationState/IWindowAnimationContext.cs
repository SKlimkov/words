﻿public interface IWindowAnimationContext
{
    void OnShowingAnimationAborted();
    void OnHidingAnimationAborted();
}