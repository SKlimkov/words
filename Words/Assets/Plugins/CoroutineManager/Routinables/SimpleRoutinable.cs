﻿namespace CoroutineHandling
{
    public struct SimpleRoutinable : IRoutinableSimple
    {
        private System.Action m_Action;
        public SimpleRoutinable(System.Action action)
        {
            m_Action = action;
        }

        public void OnAction()
        {
            m_Action();
        }
    }
}