﻿namespace RuntimeSerialization
{
    public interface ISerializationData
    {

    }

    public interface ISerializator<TResult>
    {
        TResult SerializeObject<T>(T serializableObject);
        T DeserializeObject<T>(TResult serilizedBytes);
    }
}