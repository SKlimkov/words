﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class ConvertExtensions
{
    #region FLOAT_TO_SHORT

    public static ushort EncodeToShort(this float value)
    {
        //Debug.LogFormat("EncodeToShort {0}", value);
        value = (float)Math.Round(value, 4);

        int isPositive = value > 0 ? 1 : 0;
        int countMax = 7;
        int count = 0;
        var absValue = Mathf.Abs(value);

        bool isNextNeeded = true;

        while (isNextNeeded)
        {
            var floored = Math.Floor(absValue);
            bool isFloored = absValue != floored;
            isNextNeeded = isFloored;

            if (isFloored)
            {
                var tmpValue = absValue * 10f;

                if ((tmpValue < 4095f))
                {
                    absValue = tmpValue;
                    count++;

                    if ((count > countMax))
                    {
                        isNextNeeded = false;
                    }
                }
                else
                {
                    isNextNeeded = false;
                }
            }
        }

        isPositive = (isPositive << 15);
        count = (count << 12);
        var frac = (int)absValue;
        var result = (isPositive + count + frac);
        //Debug.LogFormat("EncodeToShort {0}, {1}, {2}, {3}, {4}", isPositive, count, absValue, frac, result);
        return (ushort)result;
    }

    public static float DecodeToFloat(this ushort value)
    {
        int isPositive = value >> 15;
        int count = (value >> 12);
        if (isPositive > 0)
        {
            count -= 8;
        }

        float frac = value & 0xfff;
        var result = frac;

        while (count > 0)
        {
            result /= 10f;
            count--;
        }
        result = isPositive > 0 ? result : -result;

        //Debug.LogFormat("DecodeToFloat {0}, {1}, {2}, {3}, {4}", value, isPositive, count, frac, result);
        return result;
    }

    #endregion

    #region CONVERTATION

    private static Dictionary<Type, string[]> enumOptionsDictionary = new Dictionary<Type, string[]>();

    public static string[] ToPopupOptions<TEnum>(this TEnum enumInstance)
    {
        string[] result;
        var type = typeof(TEnum);

        if (!enumOptionsDictionary.ContainsKey(type))
        {
            var entries = Enum.GetNames(type);
            result = new string[entries.Length];

            for (var i = 0; i < entries.Length; i++)
            {
                result[i] = entries[i].ToString();
            }
        }
        else
        {
            result = enumOptionsDictionary[type];
        }        

        return result;
    }    

    //public static List<TEnum> ToList<TEnum>(this Type enumType)
    //    where TEnum : struct, IConvertible
    //{
    //    if (!enumType.IsEnum)
    //    {
    //        throw new Exception(string.Format("Wrong parameter type. Expected type is Enum, but recent type isn't"));
    //    }

    //    var result = new List<TEnum>();

    //    foreach (var entry in Enum.GetValues(typeof(TEnum)))
    //    {
    //        result.Add((TEnum)entry);
    //    }

    //    return result;
    //}

    /*public static List<TEnum> ToList<TType, TEnum>(this TType enumType) 
        where TType : Type
        where TEnum : struct, IConvertible
    {
        if (!enumType.IsEnum)
        {
            throw new Exception(string.Format("Wrong parameter type. Expected type is {0}, but recent type is {1}", typeof(TEnum), typeof(TType)));
        }

        var result = new List<TEnum>();
        foreach(var entry in Enum.GetValues(typeof(TType)))
        {
            result.Add((TEnum)entry);
        }

        return result;
    }*/

    public static TEnum? ToEnum<TEnum>(this string value) where TEnum : struct, System.IConvertible
    {
        if (!typeof(TEnum).IsEnum)
        {
            UnityEngine.Debug.LogErrorFormat("T must be an enumerated type. Value {0} is invalid", value);
            return default(TEnum);
        }

        if (!System.Enum.IsDefined(typeof(TEnum), value))
            return null;

        return (TEnum)System.Enum.Parse(typeof(TEnum), value, true);
    }

    public static int GenericEnumToInt<TEnum>(this TEnum value)
        where TEnum : struct, IConvertible
    {
        Enum parsedEnum = Enum.Parse(typeof(TEnum), value.ToString()) as Enum;
        return Convert.ToInt32(parsedEnum);
    }

    public static TEnum ToEnumNonNullable<TEnum>(this string value) where TEnum : struct, System.IConvertible
    {
        if (!typeof(TEnum).IsEnum)
        {
            UnityEngine.Debug.LogErrorFormat("T must be an enumerated type. Value {0} is invalid", value);
            return default(TEnum);
        }

        if (!System.Enum.IsDefined(typeof(TEnum), value))
        {
            UnityEngine.Debug.LogErrorFormat("Can't convert value {0} to {1}", value, typeof(TEnum));
            return default(TEnum);
        }


        return (TEnum)System.Enum.Parse(typeof(TEnum), value, true);
    }

    public static bool IsValueValidInteger(string s, out int? i)
    {
        try
        {
            i = System.Int32.Parse(s);
            return true;
        }
        catch (System.FormatException e)
        {
            i = null;
            UnityEngine.Debug.Log(i + " " + s);
            UnityEngine.Debug.Log(e.Message);
            return false;
        }
    }

    public static int ToInt(this string value)
    {
        int? tmpValue = null;
        try
        {
            tmpValue = System.Int32.Parse(value);
        }
        catch (System.FormatException e)
        {
            UnityEngine.Debug.Log(e.Message);
        }

        int result = tmpValue ?? 0;
        return result;
    }        

    public static int ToInt(this bool value)
    {
        return value ? 1 : 0;
    }        

    #endregion

    /// <summary>
    /// Creates a relative path from one file or folder to another.
    /// </summary>
    /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
    /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
    /// <returns>The relative path from the start directory to the end path or <c>toPath</c> if the paths are not related.</returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="UriFormatException"></exception>
    /// <exception cref="InvalidOperationException"></exception>
    public static String MakeRelativePath(this String toPath, String fromPath)
    {
        if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
        if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

        Uri fromUri = new Uri(fromPath);
        Uri toUri = new Uri(toPath);

        if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

        Uri relativeUri = fromUri.MakeRelativeUri(toUri);
        String relativePath = Uri.UnescapeDataString(relativeUri.ToString());

        if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
        {
            relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
        }

        return relativePath;
    }

    public static long GetSize(this object obj)
    {
        long size = 0;
        using (Stream stream = new MemoryStream())
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            size = stream.Length;
            return size;
        }
    }
}
