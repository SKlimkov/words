﻿using CustomFactory;
using Zenject;
using ZenjectServices;

namespace CustomListener
{
    public class EventTrigger<TParam> : IEventTrigger, IConfigurateable<EventTrigger<TParam>.Config>, IDeinitializeable
        where TParam : IEventParamBase
    {
        public virtual void Configurate(Config config)
        {
            m_ListenersHolder = config.listenersHolder;
        }

        public virtual void Deinitialize()
        {
            m_ListenersHolder = null;
        }

        #region INITALIZATION

        public EventTrigger() { /*UnityEngine.Debug.LogErrorFormat("Create");*/ }

        #endregion

        #region TRIGGER

        private ListenersAggregator<TParam> m_ListenersHolder;

        public void Execute(IEventParamBase param)
        {
            if (param.GetType() == typeof(TParam))
            {
                Execute((TParam)param);
            }
            else
            {
                throw new System.Exception(string.Format("Wrong argumet type. Expected type is {0}, but received type is {1}", typeof(TParam), param.GetType()));
            }
        }

        public void Execute(TParam param)
        {
            m_ListenersHolder.Execute(param);
        }

        public bool IsNeedToClear { get { return m_ListenersHolder.IsNeedToClear; } }

        #endregion

        #region NESTED_TYPES

        public struct Config
        {
            public ListenersAggregator<TParam> listenersHolder;
        }

        public class Factory : PlaceHolderFactoryWrapper<Config, EventTrigger<TParam>>
        {
            public Factory(DiContainer container) : base(container) { }

            protected override EventTrigger<TParam> CreateNewInstance(EventTrigger<TParam>.Config config)
            {
                return new EventTrigger<TParam>();
            }
        }

        #endregion
    }
}