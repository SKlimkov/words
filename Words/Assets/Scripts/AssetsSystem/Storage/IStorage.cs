﻿using System.Collections.Generic;

namespace AssetsSystem.StorageSystem
{
    public interface IStorage<TStorable>
    {
        IEnumerable<TStorable> Enumerate();
        void Add(TStorable storable);
        int Count { get; }
    }

    public class Storage<TStored> : IStorage<TStored>, IInitializeable<List<TStored>>
    {
        private List<TStored> m_Stored;

        public IEnumerable<TStored> Enumerate()
        {
            foreach (var entry in m_Stored)
            {
                yield return entry;
            }
        }
        public void Add(TStored storable)
        {
            m_Stored.Add(storable);
        }
        public void Initialize(List<TStored> stored)
        {
            m_Stored = stored;
        }

        public int Count { get { return m_Stored.Count; } }
    }
}