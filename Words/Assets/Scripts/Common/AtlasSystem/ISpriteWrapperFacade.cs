﻿using UnityEngine;

namespace AtlasSystem
{
    public enum ERenderingType
    {
        SpriteRendrer,
        Image
    };

    public interface ISpriteWrapperFacade
    {
        void SetSprite(ERenderingType type, Component wrapper, Sprite sprite);
        Component GetWrapper(GameObject go, ERenderingType wrapperType);
    }
}