﻿using CustomFactory;
using UnityEngine;
using Zenject;

namespace ZenjectServices
{
    public abstract class PlaceHolderFactory<TConfig, TResult> : IPlaceHolderFactoryWrapper<TConfig, TResult>
        where TResult : IConfigurateable<TConfig>
    {
        public PlaceHolderFactory(DiContainer container)
        {
            Container = container;

            var type = typeof(TResult);

            if (!type.IsAbstract)
            {
                TypeAnalyzer.GetInfo(type);
            }
        }

        public abstract TResult Create(TConfig config);
        public abstract TResult Create(TConfig config, GameObject prefab);

        protected DiContainer Container { get; private set; }
    }
}