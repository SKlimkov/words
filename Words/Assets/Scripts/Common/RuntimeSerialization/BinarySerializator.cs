﻿using System;
using System.IO;
//using EventAggregatorCustom.Events.Network;
using ProtoBuf;
using UnityEngine;

namespace RuntimeSerialization
{
    public class BinarySerializator : ISerializator<byte[]>
    {
        public BinarySerializator()
        {
            ProtoBuf.Meta.RuntimeTypeModel.Default.AutoCompile = false;
        }

        public byte[] SerializeObject<T>(T serializableObject)
        {
            //Debug.LogFormat("SerializeObject {0}, {1}, {2}", serializableObject, serializableObject.GetType(), typeof(T));

            using (MemoryStream mStream = new MemoryStream())
            {
                Serializer.Serialize<T>(mStream, serializableObject);
                return mStream.ToArray();
            }
        }

        public T DeserializeObject<T>(byte[] serilizedBytes)
        {
            using (MemoryStream mStream = new MemoryStream(serilizedBytes))
            {
                return Serializer.Deserialize<T>(mStream);
            }
        }
        /// <summary>
        /// IL2CPP needs only this line.
        /// </summary>
        public void UsedOnlyForAOTCodeGeneration()
        {
            /*SerializeObject(new NetworkCommandSerializationWrapper());
            SerializeObject(new ProjectilePositionResolving.SerializationWrapper());

            DeserializeObject<NetworkCommandSerializationWrapper>(new byte[0]);
            DeserializeObject<AllPlayersPreparedEvent.Message>(new byte[0]);
            DeserializeObject<BattleStartEvent.Message>(new byte[0]);
            DeserializeObject<BattleCompleteEvent.Message>(new byte[0]);
            DeserializeObject<DeployCardEvent.Message>(new byte[0]);
            DeserializeObject<LaunchProjectileEvent.Message>(new byte[0]);
            DeserializeObject<MatchLookingCompleteEvent.Message>(new byte[0]);
            DeserializeObject<NetworkPauseEvent.Message>(new byte[0]);
            DeserializeObject<PlayerConnectedEvent.Message>(new byte[0]);
            DeserializeObject<PlayersInitializationCallbackEvent.Message>(new byte[0]);
            DeserializeObject<PointCapturedEvent.Message>(new byte[0]);
            DeserializeObject<InitDeckEvent.Message>(new byte[0]);
            DeserializeObject<SceneLoadedEvent.Message>(new byte[0]);
            DeserializeObject<ProjectilePositionResolving.SerializationWrapper>(new byte[0]);
            DeserializeObject<ServerConnectionCallbackEvent.Message>(new byte[0]);
            DeserializeObject<AnimationAtlasUtility.Serialization.AnimationAtlasInfoSimple>(new byte[0]);
            DeserializeObject<AnimationAtlasUtility.Serialization.AnimationAtlasInfoDirectioned>(new byte[0]);*/

            //AnimationAtlasInfoSimple
            //AnimationAtlasInfoDirectioned

            // Include an exception so we can be sure to know if this method is ever called.
            throw new InvalidOperationException("This method is used for AOT code generation only. Do not call it at runtime.");
        }
    }
}