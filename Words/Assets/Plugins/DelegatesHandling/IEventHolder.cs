﻿namespace DelegatesHandling
{
    public interface IEventHolder<THandler>
    {
        void AddListener(THandler handler);
        void RemoveListener(THandler handler);
        void ClearListeners();
    }
}