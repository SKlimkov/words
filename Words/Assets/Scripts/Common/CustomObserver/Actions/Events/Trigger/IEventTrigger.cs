﻿namespace CustomListener
{
    /// <summary>
    /// Interface for Event trigger. Has upper level abstractions that better to use, if you want to implement this.
    /// </summary>
    public interface IEventTrigger
    {
        /// <summary>
        /// Signature for event execution method. Next level of abstraction have a method for recognize the type of paramether, and call listener as generic.
        /// </summary>
        /// <param name="param">Generalized (non-generic) execution parameter. </param>
        void Execute(IEventParamBase param);

        /// <summary>
        /// This property define if need to clear all observers of event immediatelly after event execution. 
        /// If property returns true, then you need add observer again after event execution. 
        /// If property return false, observer will call ever event execution.
        /// </summary>
        bool IsNeedToClear { get; }
    }
}