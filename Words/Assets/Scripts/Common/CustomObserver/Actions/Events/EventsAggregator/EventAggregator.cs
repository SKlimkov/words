﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CustomListener
{
    public interface IEventIdResolver
    {
        int ResolveId(Type type);
    }

    public interface IEventFactory
    {
        EventBase CreateInstance(IEventIdResolver idResolver);
    }

    public interface IEventFactory<TEvent> : IEventFactory where TEvent : EventBase { }

    public interface IEventAbstractFactory
    {
        EventBase CreateInstance(Type type, IEventIdResolver idResolver);
    }

    public class EventIdResolver : IEventIdResolver
    {
        private Dictionary<Type, int> m_IdDict;

        public EventIdResolver()
        {
            m_IdDict = new Dictionary<Type, int>();
            Initialize();
        }

        private void Initialize()
        {
            int id = 0;

            var baseType = typeof(IEvent);
            var derivedList = ReflectionHelper.EnumeratePossibleToAssignTo(baseType).ToList();

            foreach (var entry in derivedList)
            {
                if (entry.IsChildOf(typeof(IEvent)))
                {
                    m_IdDict.Add(entry, id);

                    //Debug.LogFormat("{0}, {1}", entry, id);
                    id++;
                }
            }
        }

        public int ResolveId(Type type)
        {
            if (m_IdDict.ContainsKey(type))
            {
                return m_IdDict[type];
            }
            else
            {
                throw new Exception(string.Format("Can't find id for type {0}!", type));
            }
        }
    }

    public class EventAbstractFactory : IEventAbstractFactory
    {
        private Dictionary<Type, IEventFactory> m_FactoryDict;

        public EventAbstractFactory()
        {
            m_FactoryDict = new Dictionary<Type, IEventFactory>();

            var baseType = typeof(IEvent);
            var derivedTypes = ReflectionHelper.EnumeratePossibleToAssignTo(baseType).ToList();

            foreach (var derived in derivedTypes)
            {
                var factoryGenericInterface = typeof(IEventFactory<>).MakeGenericType(derived);
                var factoryType = ReflectionExtensions.GetNestedType(derived, factoryGenericInterface, "Factory");

                var factoryInstance = Activator.CreateInstance(factoryType);

                if (!m_FactoryDict.ContainsKey(factoryType))
                {
                    m_FactoryDict.Add(derived, factoryInstance as IEventFactory);
                }
                else
                {
                    throw new Exception(string.Format("Factory for type {0} already exists in dictionary", derived));
                }
            }
        }

        public EventBase CreateInstance(Type type, IEventIdResolver idResolver)
        {
            if (m_FactoryDict.ContainsKey(type))
            {
                var factory = m_FactoryDict[type];
                //Debug.LogFormat("CreateInstance {0}, {1}", type, factory.GetType());
                return factory.CreateInstance(idResolver);
            }
            else
            {
                throw new Exception(string.Format("Can't find factory for event type {0}!", type));
            }
        }
    }

    public class EventAggregator<TEventParam> : IEventsAggregator where TEventParam : IEventParamBase
    {
        #region INITIALIZATION

        static EventAggregator()
        {
            m_EventIdResolver = new EventIdResolver();
            m_EventFactory = new EventAbstractFactory();
        }

        public EventAggregator()
        {
            //Размер словаря на две единицы больше, чем количество ивентов, наследуемых от EventBase (30 шт), в случе, если появятся новые ивенты, нужно увеличивать 
            //размер словаря при инициализации, дабы избежать ресайза словаря при его заполнении. Данное решение позволяет избежать выделения 14кБ памяти на паяку скелетов.
            m_EventDict = new Dictionary<Type, EventBase>(32);
            Configurate();
        }

        public void Configurate()
        {
            Deinitialize();

            HashSet<Type> eventList = GetEventList();

            foreach (var entry in eventList)
            {
                //Debug.LogFormat("Configurate 1 {0}, {1}", entry, entry.IsChildOf(typeof(IEvent)));

                if (entry.IsChildOf(typeof(IEvent)))
                {                    
                    var entryBaseType = entry.BaseType;

                    //Debug.LogFormat("Configurate 2 {0}, {1}", entry, entryBaseType);
                    if (entryBaseType.IsGenericType)
                    {
                        var genericParameter = ReflectionHelper.GetGenericArgument(entryBaseType)[0];

                        if (!m_EventDict.ContainsKey(genericParameter))
                        {
                            var eventInstance = CreateEventInstance(entry);
                            m_EventDict.Add(genericParameter, eventInstance);

                            //Debug.LogFormat("--------{0}, {1}, {2}", entry, eventInstance.GetType(), genericParameter);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// В этом методе нужно с помощью рефлексии получить список всех ивентов, которые используют param,
        /// наследуемый от TEventParam
        /// </summary>
        /// <returns></returns>
        private HashSet<Type> GetEventList()
        {
            HashSet<Type> result = new HashSet<Type>();

            var baseType = typeof(IEvent);
            var derivedList = ReflectionHelper.EnumeratePossibleToAssignTo(baseType).ToList();

            //Debug.LogFormat("GetEventList {0}, {1}, {2}", derivedList.Count, baseType, typeof(TEventParam));

            foreach (var entry in derivedList)
            {
                //Debug.LogFormat("GetEventList {0}, {1}", entry, entry.IsChildOf(typeof(IEvent)));

                if (entry.IsChildOf(typeof(IEvent)))
                {
                    var entryBaseType = entry.BaseType;
                    if (entryBaseType.IsGenericType)
                    {
                        var genericParameter = ReflectionHelper.GetGenericArgument(entryBaseType)[0];

                        if (genericParameter.IsChildOf(typeof(TEventParam)))
                        {
                            result.Add(entry);
                        }
                    }
                }
            }

            return result;
        }

        #endregion

        #region EVENT_INSTANTIATION

        protected static IEventIdResolver m_EventIdResolver;
        protected static IEventAbstractFactory m_EventFactory;

        private EventBase CreateEventInstance(Type instanceType)
        {
            //var result = Activator.CreateInstance(instanceType, m_EventIdResolver) as EventBase;
            var result = m_EventFactory.CreateInstance(instanceType, m_EventIdResolver);
            return result;
        }

        #endregion

        #region EVENT_AGGREGATOR

        private Dictionary<Type, EventBase> m_EventDict;

        public IEvent GetEventByParamType(Type type)
        {
            //Debug.LogFormat("GetEventByParamType {0}, {1}", type, type.Name);
            if (!m_EventDict.ContainsKey(type))
            {
                throw new Exception(string.Format("Can not find listener with key {0} in dictionary", type));
            }

            //Debug.LogFormat("GetEventByParamType {0}, {1}", type, m_EventDict[type].GetType());

            return m_EventDict[type];
        }

        public IEnumerable<IEvent> EnumerateEvents()
        {
            foreach (var entry in m_EventDict)
            {
                yield return entry.Value;
            }
        }

        #endregion

        #region DEINITIALIZATION

        public void Deinitialize()
        {
            foreach (var entry in m_EventDict)
            {
                entry.Value.Deinitialize();
            }

            m_EventDict.Clear();
        }

        #endregion        
    }
}