﻿using UnityEngine;
using Zenject;

namespace CoroutineHandling
{
    /// <summary>
    /// Interface for IRoutinable implementations.
    /// </summary>
    public interface ICoroutineService
    {
        MonoBehaviourCustom MonoInstance { get; }

        MonoBehaviourCustom GetMonoInstance(ELazyActionType actionType);
    }

    /// <summary>
    /// This class is used as MonoInstance with all IRoutinable implementations, who do not have their own MonoBehaviour instance
    /// </summary>
    public class CoroutineService : MonoBehaviourCustom, ICoroutineService
    {
        /// <summary>
        /// Property for storing a link to MonoBehaviour instance. Used for call StartCoroutine method.
        /// </summary>       

#pragma warning disable 649
        [SerializeField] private MonoBehaviourCustom m_MonoInstanceNull;
        [SerializeField] private MonoBehaviourCustom m_MonoInstanceEndOfFrame;
        [SerializeField] private MonoBehaviourCustom m_MonoInstanceFixedUpdate;
#pragma warning restore 649

        public MonoBehaviourCustom MonoInstance { get { return this; } }

        public MonoBehaviourCustom GetMonoInstance(ELazyActionType actionType)
        {
            switch (actionType)
            {
                case ELazyActionType.Null:
                    {
                        return m_MonoInstanceNull;
                    }
                case ELazyActionType.EndOfFrame:
                    {
                        return m_MonoInstanceEndOfFrame;
                    }
                case ELazyActionType.FixedUpdate:
                    {
                        return m_MonoInstanceFixedUpdate;
                    }
                default:
                    {
                        return MonoInstance;
                    }
            }
        }
    }
}