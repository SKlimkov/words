﻿namespace CustomListener
{
    /// <summary>
    /// Generalized non-generic interface for all events. 
    /// </summary>
    public interface IEvent : IEventMember
    {
        IEventTrigger Trigger { get; }
        IListenersAggregator ListenersAggregator { get; }
    }
}