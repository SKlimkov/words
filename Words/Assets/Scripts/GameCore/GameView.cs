﻿using AssetsSystem;
using AssetsSystem.StorageSystem;
using CoroutineHandling;
using CustomListener;
using CustomUi;
using GameSystems.EventSystem;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public enum EShowLanguage
{
    English = 0,
    Russian = 1
};

public struct WordShowingConfig
{
    public Word word;
    public EShowLanguage language;
}

public class GameView : MonoBehaviour
{
    #region SERIALIZEABLE_VARIABLES
#pragma warning disable 649
    [SerializeField] private Text           m_ShownWord;
    [SerializeField] private Text           m_AnswerWord;
    [SerializeField] private InputField     m_Input;
    [SerializeField] private Image          m_AnswerIndicator;
    [SerializeField] private Button         m_ConfirmButton;
    [SerializeField] private Text           m_ButtonText;
    [SerializeField] private Button         m_CloseButton;

    [SerializeField] private Color          m_SuccessColor;
    [SerializeField] private Color          m_FailColor;
    [SerializeField] private Color          m_DefaultColor;
#pragma warning restore 649
    #endregion

    #region INITIALIZATION 

    [Inject]
    private void ResolveDependencies(DiContainer container)
    {
        var systemEventFasade = container.Resolve<PublisherFacade<SystemEventFacade.IEventParam>>();
        systemEventFasade.AddEventListener<SystemEventFacade.OnGameSessionNeedToStart.Param>(OnGameSessionNeedToStart);

        m_AssetManager = container.Resolve<IAssetManager>();
        m_MonoInstance = container.ResolveId<ICoroutineService>("CoroutineService").MonoInstance;

        m_UiEventsFasade = container.Resolve<PublisherFacade<UiEventFacade.IEventParam>>();
    }

    private void Awake()
    {
        m_CloseButton.onClick.AddListener(OnCloseButtonClick);
    }

    #endregion

    #region DEINITIALIZATION

    private void Deinitialize()
    {
        m_ConfirmButton.onClick.RemoveAllListeners();
        m_Input.onEndEdit.RemoveListener(OnInputComplete);
    }

    private void OnDestroy()
    {
        m_CloseButton.onClick.RemoveListener(OnCloseButtonClick);
    }

    #endregion

    #region CLOSE_BUTTON

    private PublisherFacade<UiEventFacade.IEventParam> m_UiEventsFasade;

    private void OnCloseButtonClick()
    {
        m_UiEventsFasade.TriggerEvent(new UiEventFacade.ShowWindowEvent.Param("MainMenuWindow", new MainMenuWindow.ShowData()));
        m_UiEventsFasade.TriggerEvent(new UiEventFacade.HideWindowEvent.Param("GameWindow", new GameViewWindow.HideData()));
    }

    #endregion

    private IAssetManager m_AssetManager;
    private MonoBehaviour m_MonoInstance;

    /// <summary>
    /// Здесь нужно добавить функционал, который будет собирать все помеченные сеты в отдельный объект.
    /// В дальнейшем показанные слова должны исключаться из рандома.
    /// </summary>
    /// <param name="param"></param>
    private void OnGameSessionNeedToStart(SystemEventFacade.OnGameSessionNeedToStart.Param param)
    {
        Debug.LogFormat("OnGameSessionNeedToStart");

        ShowWord(GetWordConfig(m_AssetManager.GetDecoratorForFile(AssetUtility.SettingAsset.SheetId).AssetStorage));
    }

    /// <summary>
    /// Сейчас показ слов происходит в случаном порядке бесконечно. Нужно перевести все это на сессионный режим. 
    /// При запуске сессии должен формироваться хэшсет, включающий в себя все слова из всех ассетов, и по мере показа слова должны из хэшсета исключаться.
    /// В итоге алгоритм должен показать все слова, выбранные к показу. 
    /// </summary>
    private WordShowingConfig GetWordConfig(AssetStorage<WordsSetAsset> storage)
    {
        var assetList = storage.Enumerate().Where(x => x.IsChoosed).ToList();

        var asset = assetList[Random.Range(0, assetList.Count)];

        var word = asset.Words[Random.Range(0, asset.Words.Count)];        

        var config = new WordShowingConfig()
        {
            word = word,
            language = (EShowLanguage)Random.Range(0, 2)
        };

        return config;
    }

    private string m_CurrentRightAnswerHolder;

    private void ShowWord(WordShowingConfig config)
    {
        Debug.LogFormat("ShowWord");

        string shownWord;

        if (config.language == EShowLanguage.English)
        {
            shownWord = config.word.english;
            m_CurrentRightAnswerHolder = config.word.russian;
        }
        else
        {
            shownWord = config.word.russian;
            m_CurrentRightAnswerHolder = config.word.english;
        }

        m_ShownWord.text = shownWord;
        m_AnswerWord.text = "";
        m_Input.text = "";
        m_ButtonText.text = "Confirm";

        m_LastGottedAnswer = null;
        m_AnswerIndicator.color = m_DefaultColor;

        m_ConfirmButton.onClick.AddListener(ConfirmButtonClickHandler);

        m_Input.onEndEdit.AddListener(OnInputComplete);
    }

    private string m_LastGottedAnswer;

    private void OnInputComplete(string answer)
    {
        m_LastGottedAnswer = answer;
    }    

    private void ConfirmButtonClickHandler()
    {
        m_ConfirmButton.onClick.RemoveListener(ConfirmButtonClickHandler);

        if (!string.IsNullOrEmpty(m_LastGottedAnswer))
        {
            bool isSuccess = false;
            if (m_CurrentRightAnswerHolder == m_LastGottedAnswer)
            {
                isSuccess = true;
            }

            if (isSuccess)
            {
                m_AnswerIndicator.color = m_SuccessColor;
            }
            else
            {
                m_AnswerIndicator.color = m_FailColor;
            }

            m_AnswerWord.text = m_CurrentRightAnswerHolder;
            m_ButtonText.text = "Next";
            m_ConfirmButton.onClick.AddListener(NextButtonClickHandler);
        }        
    }

    private void NextButtonClickHandler()
    {
        m_ConfirmButton.onClick.RemoveListener(NextButtonClickHandler);

        ShowWord(GetWordConfig(m_AssetManager.GetDecoratorForFile(AssetUtility.SettingAsset.SheetId).AssetStorage));
        Debug.LogFormat("Next");       

        m_ConfirmButton.onClick.AddListener(ConfirmButtonClickHandler);
    }
}