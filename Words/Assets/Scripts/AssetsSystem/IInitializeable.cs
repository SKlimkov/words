﻿namespace AssetsSystem
{
    public interface IInitializeable<Config>
    {
        void Initialize(Config config);
    }
}