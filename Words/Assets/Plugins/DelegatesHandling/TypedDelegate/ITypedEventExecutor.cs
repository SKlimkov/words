﻿namespace DelegatesHandling
{
    public interface ITypedEventExecutor<TEventResult, TEventData>
        where TEventResult : IEventResult
        where TEventData : IEventMessage
    {
        TEventResult Execute(TEventData data);
    }
}