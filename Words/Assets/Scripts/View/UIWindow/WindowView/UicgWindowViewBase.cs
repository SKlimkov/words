﻿using DelegatesHandling;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class UicgWindowViewBase : MonoBehaviour, IWindowView
{
    private CanvasGroup m_Cg;

    #region INITIALIZATION

    private void Awake()
    {
        m_Cg = GetComponent<CanvasGroup>();

        VoidEventHolder showHolder = new VoidEventHolder();
        OnShowingUp = showHolder;
        m_OnShowingUpExecutor = showHolder;

        VoidEventHolder hideHolder = new VoidEventHolder();
        OnHidingUp = hideHolder;
        m_OnHidingUpExecutor = hideHolder;
    }

    #endregion

    #region WINDOW

    public void Show()
    {
        //Debug.LogErrorFormat("Show {0}", gameObject.name);
        m_Cg.alpha = 1;
        m_Cg.interactable = true;
        m_Cg.blocksRaycasts = true;

        m_OnShowingUpExecutor.Execute();
    }

    public void Hide()
    {
        //Debug.LogErrorFormat("Hide {0}", gameObject.name);
        m_Cg.alpha = 0;
        m_Cg.interactable = false;
        m_Cg.blocksRaycasts = false;

        m_OnHidingUpExecutor.Execute();
    }

    public void Lock()
    {
        m_Cg.interactable = false;
    }

    public void Unlock()
    {
        m_Cg.interactable = true;
    }

    #endregion

    #region EVENTS

    public IEventHolder<VoidDelegate> OnShowingUp { get; private set; }
    private IEventExecutor<VoidDelegate> m_OnShowingUpExecutor;

    public IEventHolder<VoidDelegate> OnHidingUp { get; private set; }
    private IEventExecutor<VoidDelegate> m_OnHidingUpExecutor;

    #endregion
}