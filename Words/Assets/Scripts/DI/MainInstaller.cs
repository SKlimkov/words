using AssetsSystem;
using AssetsSystem.StorageSystem;
using CustomListener;
using CustomUi;
using GameSystems.EventSystem;
using GoogleRemoteSync;
using OneLineNamespace;
using ResourceLoading;
using RuntimeSerialization;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MainInstaller : MonoInstaller<MainInstaller>
{
    public override void InstallBindings()
    {
        InstallResources();
        InstallMisc();
        InstallWindows();
        InstallEvents();
        InstallSync();
        InstallBootstrap();
    }

    private void InstallResources()
    {
        Container.Bind<PrefabLoadingManager>().ToSelf().AsSingle();
        Container.Bind<ScriptableObjectManager>().ToSelf().AsSingle();
        Container.Bind<SpriteManager>().ToSelf().AsSingle();
        Container.Bind<Texture2DManager>().ToSelf().AsSingle();
        Container.Bind<FontManager>().ToSelf().AsSingle();
    }

    private void InstallMisc()
    {
        Container.Bind<PlatformDependencyResolver>().ToSelf().AsSingle();
        Container.Bind<ISerializator<byte[]>>().To<BinarySerializator>().AsSingle();
        Container.Bind<UniRx.RoutineHelper>().ToSelf().AsSingle();        
    }

    private void InstallBootstrap()
    {
        Container.Bind<Zenject.IInitializable>().To<Bootstrap>().AsSingle();
    }

    private void InstallSync()
    {
        Container.Bind<ISyncManager>().To<SyncManager>().AsSingle();
        Container.Bind<FileAssetDecorator.Storage>().ToSelf().AsTransient();
        Container.Bind<AssetStorage<WordsSetAsset>>().ToSelf().AsTransient();
        Container.Bind<IRevisionVerifyingManager>().To<RevisionVerifyingManager>().AsSingle();

        //Container.Bind<System.IObserver<RevisionVerifyingManager.Result>>().To<RevisionVerifyingManager.Observer>().AsTransient();
        //Container.Bind<IFileAssetDecorator>().To<FileAssetDecorator>().AsSingle();

        Container.Bind<IAssetManager>().To<AssetManager>().AsSingle();
    }

    private void InstallEvents()
    {
        Container.Bind<PublisherFacade<UiEventFacade.IEventParam>>().To<UiEventFacade>().AsSingle();
        Container.Bind<PublisherFacade<SystemEventFacade.IEventParam>>().To<SystemEventFacade>().AsSingle();
    }

    private void InstallWindows()
    {
        Container.Bind<WindowPrefabsHolder>().ToSelf().AsSingle();
        Container.Bind<IWindowManager>().To<WindowManagerCustom>().AsSingle();

        Container.Bind<WorkSheetView.Factory>().ToSelf().AsSingle();        
    }
}