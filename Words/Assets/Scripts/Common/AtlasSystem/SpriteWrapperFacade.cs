﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AtlasSystem
{
    public class SpriteWrapperFacade : ISpriteWrapperFacade
    {
        static SpriteWrapperFacade()
        {
            m_Wrappers = new Dictionary<int, ISpriteWrapperAdapter>();
            m_Wrappers.Clear();
            m_Wrappers.Add((int)ERenderingType.SpriteRendrer, new SpriteRendererAdapter());
            m_Wrappers.Add((int)ERenderingType.Image, new ImageAdapter());
        }

        private static Dictionary<int, ISpriteWrapperAdapter> m_Wrappers;

        public void SetSprite(ERenderingType type, Component wrapper, Sprite sprite)
        {
            ISpriteWrapperAdapter adapter;
            var result = m_Wrappers.TryGetValue((int)type, out adapter);

            if (result)
            {
                adapter.SetSprite(wrapper, sprite);
            }
            else
            {
                throw new System.Exception(string.Format("Can't find adapter for wrappe type {0}", wrapper.GetType()));
            }
        }

        public Component GetWrapper(GameObject go, ERenderingType wrapperType)
        {
            switch (wrapperType)
            {
                case ERenderingType.Image:
                    return go.GetComponent<Image>();
                case ERenderingType.SpriteRendrer:
                    return go.GetComponent<SpriteRenderer>();
                default:
                    throw new System.Exception("IMPOSSIBRU");
            }
        }

        public abstract class SpriteWrapperAdapter<TWrapper> : ISpriteWrapperAdapter<TWrapper>
            where TWrapper : Component
        {
            public void SetSprite(Component wrapper, Sprite sprite)
            {
                if (wrapper as TWrapper)
                {
                    SetSprite(wrapper as TWrapper, sprite);
                }
                else
                {
                    throw new System.Exception(string.Format("Invalid wrapper type. Recent type is {0}, but expected type is {1}", wrapper.GetType(), typeof(TWrapper)));
                }

            }
            public abstract void SetSprite(TWrapper wrapper, Sprite sprite);
        }

        public class SpriteRendererAdapter : SpriteWrapperAdapter<SpriteRenderer>
        {
            public override void SetSprite(SpriteRenderer wrapper, Sprite sprite)
            {
                wrapper.sprite = sprite;
            }
        }

        public class ImageAdapter : SpriteWrapperAdapter<Image>
        {
            public override void SetSprite(Image wrapper, Sprite sprite)
            {
                wrapper.sprite = sprite;
            }
        }
    }
}