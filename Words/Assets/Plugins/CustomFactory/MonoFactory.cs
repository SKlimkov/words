﻿using UnityEngine;
using ZenjectServices;

namespace CustomFactory
{
    public interface IMonoFactory<TConfig, TResult>
        where TResult : IConfigurateable<TConfig>
        where TConfig: IMonoConfigWrapper
    {
        TResult Create(TConfig config, GameObject prefab);
    }

    public class MonoFactory<TConfig, TResult> : IMonoFactory<TConfig, TResult>
         where TResult : IConfigurateable<TConfig>
        where TConfig : IMonoConfigWrapper
    {
        public MonoFactory(Zenject.DiContainer container)
        {
            Container = container;
        }

        protected Zenject.DiContainer Container { get; private set; }

        public virtual TResult Create(TConfig config, GameObject prefab)
        {
            var result = CreateInstance(prefab, config.MonoConfig);
            if (result == null)
            {
                Debug.LogErrorFormat("MonoFactory {0}, {1}, {2}", prefab, typeof(TConfig), typeof(TResult));
            }
            result.Configurate(config);
            return result;
        }

        protected virtual void ConfigurateMono(GameObject instance, MonoConfig monoConfig)
        {
            var transform = instance.transform;

            if (monoConfig.hasParent)
            {
                transform.SetParent(monoConfig.root);
            }

            transform.position = monoConfig.position;
            transform.rotation = monoConfig.rotation;
            transform.localScale = monoConfig.localScale;
        }

        private TResult CreateInstance(GameObject prefab, MonoConfig monoConfig)
        {
            bool shouldMakeActive;
            var instance = InstantiatePrefab(prefab, out shouldMakeActive);

            InjectGameObject(instance);

            if (shouldMakeActive)
            {
                instance.SetActiveSafe(true);
            }

            ConfigurateMono(instance, monoConfig);

            //Debug.LogFormat("----------- {0}, {1}", typeof(TResult), monoConfig.hasParent);

            var result = instance.GetComponent<TResult>();

            if (result == null)
            {
                throw new System.Exception(string.Format("Prefab do not containt needed component {0}!", typeof(TResult)));
            }

            return result;
        }

        protected GameObject InstantiatePrefab(GameObject prefab, out bool shouldMakeActive)
        {
            if (prefab == null)
            {
                throw new System.Exception("Prefab is null!");
            }

            var wasActive = prefab.activeSelf;

            if (wasActive)
            {
                prefab.SetActiveSafe(false);
            }

            shouldMakeActive = wasActive;

            try
            {
                var result = GameObject.Instantiate(prefab) as GameObject;
                //Debug.LogFormat("try {0}", prefab.activeSelf);
                return result;
            }
            finally
            {
                if (wasActive)
                {
                    // Always make sure to reset prefab state otherwise this change could be saved
                    // persistently
                    prefab.SetActiveSafe(true);

                    //Debug.LogFormat("finally {0}", prefab.activeSelf);
                }
            }
        }

        public void InjectGameObject(GameObject gameObject)
        {
            Container.FlushBindings();

            var children = gameObject.GetComponentsInChildren<MonoBehaviour>(true);

            foreach (var entry in children)
            {
                if (entry is IInjectable)
                {
                    (entry as IInjectable).Inject(Container);
                }
            }
        }
    }
}