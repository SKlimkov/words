﻿using AssetsSystem;
using CustomFactory;
using ResourceLoading;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using ZenjectServices;

public class WorkSheetView : MonoConfigurateable<WorkSheetView.Config>
{
    #region INITIALIZATION
    
    private Text m_SheetView;
    private Text m_SheetContentView;
    private Toggle m_ChoosToggle;
    private WordsSetAsset m_Asset;

    public override void Configurate(Config config)
    {
        var textes = GetComponentsInChildren<Text>();
        m_ChoosToggle = GetComponentInChildren<Toggle>();

        m_Asset = config.Asset;

        for (var i = 0; i < textes.Length; i++)
        {
            var entry = textes[i];
            if (entry.gameObject.name == "SheetView")
            {
                m_SheetView = entry;
            }

            if (entry.gameObject.name == "SheetContentView")
            {
                m_SheetContentView = entry;
            }
        }

        var words = "";

        for(var i = 0; i < m_Asset.Words.Count; i++)
        {
            var entry = config.Asset.Words[i];
            words = string.Format("{0} {1},", words, entry.english);
        }

        words.Remove(words.Length - 1);

        InitializeTextView(ref m_SheetView, m_Asset.Name);
        InitializeTextView(ref m_SheetContentView, words);
        m_ChoosToggle.isOn = m_Asset.IsChoosed;

        Debug.LogFormat("{0}", m_Asset.IsChoosed);

        m_ChoosToggle.onValueChanged.AddListener(isChoosed => { m_Asset.SetChoosed(isChoosed); });
    }

    private void InitializeTextView(ref Text view, string text)
    {
        if (m_SheetView == null)
        {
            throw new System.NullReferenceException();
        }

        view.text = string.Format("{0}{1}", view.text, text);
    }

    #endregion

    #region DEINITIALIZATION

    public void Deinitialize()
    {
        m_SheetView.text = "Sheet: ";
        m_SheetContentView.text = "Words: ";
    }

    #endregion

    #region NESTED_TYPES

    public struct Config : IMonoConfigWrapper
    {
        public MonoConfig MonoConfig { get; private set; }
        public WordsSetAsset Asset { get; private set; }

        public Config(MonoConfig monoConfig, WordsSetAsset asset)
        {
            MonoConfig = monoConfig;
            Asset = asset;
        }
    }

    public class Factory : MonoFactory<Config, WorkSheetView>
    {
        public Factory(DiContainer container) : base(container)
        {
            m_Prefab = container.Resolve<PrefabLoadingManager>().LoadObject(Prefabs.Ui.WorkSheetViewConfig);
        }

        protected override void ConfigurateMono(GameObject instance, MonoConfig monoConfig)
        {
            base.ConfigurateMono(instance, monoConfig);

            var rt = instance.transform as RectTransform;
            //rt.anchoredPosition = new Vectorr
            rt.anchoredPosition3D = Vector3.zero;
            //Debug.LogFormat("{0}, {1}", instance.transform.position, rt.anchoredPosition3D);
        }

        private GameObject m_Prefab;

        public WorkSheetView Create(Config config)
        {
            return Create(config, m_Prefab);
        }
    }

    #endregion
}