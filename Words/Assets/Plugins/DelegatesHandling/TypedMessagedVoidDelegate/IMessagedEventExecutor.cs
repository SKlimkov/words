﻿namespace DelegatesHandling
{
    public interface IMessagedEventExecutor<TEventData>
    {
        void Execute();
    }
}