﻿namespace CustomActions
{
    public interface IVoidExecutable
    {
        void Execute(IExecutableParam param);
    }
}