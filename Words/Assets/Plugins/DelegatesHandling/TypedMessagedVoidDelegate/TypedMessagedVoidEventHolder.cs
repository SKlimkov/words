﻿namespace DelegatesHandling
{
    public class TypedMessagedVoidEventHolder<TEventMessage> : IMessagedEventHolder<TypedVoidDelegate<TEventMessage>, TEventMessage>, IMessagedEventExecutor<TEventMessage>
            where TEventMessage : IEventMessage
    {
        private System.Collections.Generic.Dictionary<TypedVoidDelegate<TEventMessage>, TEventMessage> m_HandlersDict =
            new System.Collections.Generic.Dictionary<TypedVoidDelegate<TEventMessage>, TEventMessage>();

        public void AddListener(TypedVoidDelegate<TEventMessage> handler, TEventMessage message)
        {
            /*UnityEngine.Debug.LogErrorFormat("AddListener {0}, {1}, {2}, {3}, {4}", 
                m_HandlersDict.Count,
                message.GetType(),
                message.GetHashCode(),
                handler.GetHashCode(),
                GetHashCode());*/

            if (!m_HandlersDict.ContainsKey(handler))
            {
                m_HandlersDict.Add(handler, message);
                //UnityEngine.Debug.LogErrorFormat("AddListener {0}", m_HandlersDict.Count);
                return;
            }
            m_HandlersDict[handler] = message;
            //UnityEngine.Debug.LogFormat("Handler {0} {1} {2} already exist in dictionary.", handler, message.GetType(), GetHashCode());

            //UnityEngine.Debug.LogErrorFormat("AddListener {0}", m_HandlersDict.Count);            
        }

        public void RemoveListener(TypedVoidDelegate<TEventMessage> handler)
        {
            /*UnityEngine.Debug.LogErrorFormat("RemoveListener {0}, {1}, {2}",
                m_HandlersDict.Count,
                handler.GetHashCode(),
                GetHashCode());*/

            if (m_HandlersDict.ContainsKey(handler))
            {
                /*if (!m_HandlersToDelete.Contains(handler))
                {
                    m_HandlersToDelete.Add(handler);
                }*/                

                m_HandlersDict.Remove(handler);
                return;
            }
            UnityEngine.Debug.LogErrorFormat("Can't find handler {0} in dictionary.", handler);
        }

        public void ClearListeners()
        {
            m_HandlersDict.Clear();
            m_HandlersToDelete.Clear();
        }

        private System.Collections.Generic.List<TypedVoidDelegate<TEventMessage>> m_HandlersToDelete = 
            new System.Collections.Generic.List<TypedVoidDelegate<TEventMessage>>();

        public void Execute()
        {
            foreach (var entry in m_HandlersDict)
            {
                TypedVoidDelegate<TEventMessage> handler = entry.Key;
                handler(entry.Value);                

                if ((entry.Value != null) && (entry.Value.IsNeedToClear))
                {
                    m_HandlersToDelete.Add(handler);
                }
            }

            foreach (var entry in m_HandlersToDelete)
            {
                RemoveListener(entry);
            }

            m_HandlersToDelete.Clear();

            //UnityEngine.Debug.LogFormat("Delegate foreach {0}", m_HandlersDict.Count);
        }
    }
}