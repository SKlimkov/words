﻿public interface IWindowContext
{
    void Show(IShowWindowData data);
    void Hide(IHideWindowData data);

    string WindowId { get; }
}