﻿using UnityEngine;

namespace CustomListener
{
    /// <summary>
    /// Abstract class implemented template pattern for IPublisherEventsFacade and IPublisherTriggersFacade interfaces.
    /// Listening and executing for events and callbacks separeted by interfaces.
    /// </summary>
    public abstract class PublisherFacadeBase : IPublisherEventsFacade, IPublisherTriggersFacade
    {
        #region INITIALIZATION

        public PublisherFacadeBase()
        {
            EventAggregator = InitializeEventAggregator();
        }

        protected abstract IEventsAggregator InitializeEventAggregator();

        #endregion

        #region INTERNAL_FUNCTIONALITY

        /// <summary>
        /// Protected method for executing event py it's param.
        ///  Method get event from event aggregator by param type, and execute it with param instance.
        /// </summary>
        /// <param name="param"></param>
        protected void ExecuteEventByParam(IEventParamBase param)
        {
            EventAggregator.GetEventByParamType(param.GetType()).Trigger.Execute(param);
        }

        /// <summary>
        /// Protected property for inherited accsess to event aggregator instance.
        /// </summary>
        protected IEventsAggregator EventAggregator { get; private set; }

        #endregion

        #region CONTEXT EVENTS

        /// <summary>
        /// Generic method for adding listeners to context event by external users.
        /// </summary>
        /// <typeparam name="TParam">Generic param for event argument. Used for define for wich event the listener must be added</typeparam>
        /// <param name="listener">Addable delegate that woul'd be added as event listener.</param>
        public virtual void AddEventListener<TParam>(OnEventTriggerHandler<TParam> listener) where TParam : IEventParamBase
        {
            //UnityEngine.Debug.LogFormat("AddEventListener {0}", typeof(TParam));
            var entry = EventAggregator.GetEventByParamType(typeof(TParam));
            //UnityEngine.Debug.LogFormat("AddListener {0}, {1}, {2}", typeof(TParam), entry, entry.ListenersAggregator);
            entry.ListenersAggregator.AddListener(listener);
        }

        /// <summary>
        /// Generic method for removing listeners to context event by external users.
        /// </summary>
        /// <typeparam name="TParam">Generic param for event argument. Used for define from wich event the listener must be removed.</typeparam>
        /// <param name="listener">Removable delegate that woul'd be removed as event listener.</param>
        public virtual void RemoveEventListener<TParam>(OnEventTriggerHandler<TParam> listener) where TParam : IEventParamBase
        {
            //UnityEngine.Debug.LogFormat("RemoveListener {0}", typeof(TParam));
            var entry = EventAggregator.GetEventByParamType(typeof(TParam));
            entry.ListenersAggregator.RemoveListener(listener);
        }

        /// <summary>
        /// Generic method for event executing (triggerting) by external users.
        /// </summary>
        /// <typeparam name="TType">Generic param for event argument. Used for define from wich event the listener must be executed.</typeparam>
        /// <param name="param">Param instance with execution data.</param>
        public virtual void TriggerEvent<TType>(TType param) where TType : IEventParamBase
        {
            EventAggregator.GetEventByParamType(param.GetType()).Trigger.Execute(param);
        }

        #endregion        

        public void ClearAllListeners()
        {
            var events = EventAggregator.EnumerateEvents();

            foreach(var entry in events)
            {
                entry.ListenersAggregator.ClearAllListeners();
            }
        }
    }

    public class PublisherFacade<TEventParam> : PublisherFacadeBase where TEventParam : IEventParamBase
    {
        #region INITIALIZE

        protected override IEventsAggregator InitializeEventAggregator()
        {
            return new EventAggregator<TEventParam>();
        }

        #endregion

        #region CONTEXT_EVENTS

        public override void AddEventListener<TParam>(OnEventTriggerHandler<TParam> listener)
        {
            if (typeof(TParam).IsChildOf(typeof(TEventParam)))
            {
                base.AddEventListener(listener);
            }
            else
            {
                throw new System.Exception(
                    string.Format("Wrong type of listener's argument. Expected listener's argument must be assignable from {0}, but resent is {1}",
                    typeof(TEventParam), typeof(TParam)));
            }
        }

        public override void RemoveEventListener<TParam>(OnEventTriggerHandler<TParam> listener)
        {
            if (typeof(TParam).IsChildOf(typeof(TEventParam)))
            {
                base.RemoveEventListener(listener);
            }
            else
            {
                throw new System.Exception(
                    string.Format("Wrong type of listener's argument. Expected listener's argument must be assignable from {0}, but resent is {1}",
                    typeof(TEventParam), typeof(TParam)));
            }
        }

        public override void TriggerEvent<TType>(TType param)
        {
            //Debug.LogFormat("TriggerEvent {0}", param);
            if (typeof(TType).IsChildOf(typeof(TEventParam)))
            {
                base.TriggerEvent(param);
            }
            else
            {
                throw new System.Exception(
                    string.Format("Wrong type of listener's argument. Expected listener's argument must be assignable from {0}, but resent is {1}",
                    typeof(TEventParam), typeof(TType)));
            }
        }

        #endregion
    }
}