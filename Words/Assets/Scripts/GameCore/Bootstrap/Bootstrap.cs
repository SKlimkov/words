﻿using AssetsSystem;
using CustomListener;
using CustomUi;
using GameSystems.EventSystem;
using GoogleRemoteSync;
using System.Linq;
using UniRx.Async;
using UnityEngine;

public class Bootstrap : Zenject.IInitializable
{
    public Bootstrap(Zenject.DiContainer container)
    {
        m_SyncManager = container.Resolve<ISyncManager>();
        m_UiEventsFasade = container.Resolve<PublisherFacade<UiEventFacade.IEventParam>>();
        m_RevisionVerifyingManager = container.Resolve<IRevisionVerifyingManager>();
    }

    private ISyncManager m_SyncManager;
    private PublisherFacade<UiEventFacade.IEventParam> m_UiEventsFasade;

    private IFileAssetDecorator m_AssetStorageFacade;

    private IRevisionVerifyingManager m_RevisionVerifyingManager;

    public async void Initialize()
    {
        Debug.LogFormat("Initialize");

        m_UiEventsFasade.TriggerEvent(new UiEventFacade.ShowWindowEvent.Param("LoadingWindow", new LoadingWindow.ShowData()));

        var checkingResult = await CheckRevision();

        if (checkingResult.isRevisionUpdateNeeded)
        {
            m_UiEventsFasade.TriggerEvent(new UiEventFacade.ShowWindowEvent.Param("MainMenuWindow", new MainMenuWindow.ShowData()));
            m_UiEventsFasade.TriggerEvent(new UiEventFacade.HideWindowEvent.Param("LoadingWindow", new LoadingWindow.HideData()));

            Debug.LogFormat("OnComplete");
        }
        else
        {
            OnGooleDriveReachFailed(checkingResult.revisionVerifyResult.request);
        }
    }

    struct CheckingInfo
    {
        public RevisionVerifyingManager.Result revisionVerifyResult;
        public bool isSyncSuccess;
        public bool isRevisionUpdateNeeded;
    }

    private async UniTask<CheckingInfo> CheckRevision()
    {
        var fileId = AssetUtility.SettingAsset.SheetId;

        var revisionVerifyConfig = new RevisionVerifyingManager.Config() { sheetId = fileId };

        var revisionResult = await m_RevisionVerifyingManager.Verify(revisionVerifyConfig);

        var result = new CheckingInfo() { revisionVerifyResult = revisionResult };

        result.isRevisionUpdateNeeded = revisionResult.request != null && revisionResult.request.IsDone && !revisionResult.request.IsError;

        if (result.isRevisionUpdateNeeded)
        {
            var latestUpdate = revisionResult.info.Revisions.Max(x => x.ModifiedTime);
            Debug.LogFormat("LatestUpdate {0}", latestUpdate);

            var syncResult = await m_SyncManager.Sync(new SyncManager.Config() { fileId = fileId, lasRevision = latestUpdate });
            result.isSyncSuccess = syncResult.isSuccess;
        }

        return result;
    }

    private void LogRevisions(FileRevisionInfo.RevisionInfo[] revisions)
    {
        foreach (var entry in revisions)
        {
            Debug.LogFormat("{0}, {1}", entry.Id, entry.ModifiedTime.ToLocalTime());
        }
    }
    
    private void OnGooleDriveReachFailed(FileRevisionInfo.GetRequest request)
    {
        if (request.IsError)
        {
            Debug.LogErrorFormat(request.Error);
        }
        else
        {
            Debug.LogErrorFormat("Can't reach google drive service");
        }
    }
}