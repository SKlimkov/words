﻿namespace CustomActions
{
    public interface IResultableExecutable
    {
        IExecutableResult Execute(IExecutableParam param);
    }
}