﻿using System;
using System.Collections.Generic;

namespace GoogleSheetsToUnity
{
    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = UnityEngine.JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.values;
        }

        public static T[] FromJsonNested<T>(string json)
        {
            Wrapper<T> wrapper = UnityEngine.JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.values;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.values = array;
            return UnityEngine.JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.values = array;
            return UnityEngine.JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] values;
        }
    }

    [Serializable]
    public class ValueRange
    {
        public string range = "";
        public string majorDimension;
        public List<List<string>> values = new List<List<string>>();

        public ValueRange() { }

        /// <summary>
        /// Used to create a spreadshhet that can be returned to google sheets
        /// </summary>
        /// <param name="data"></param>
        public ValueRange(List<List<string>> data)
        {
            values = data;
        }

        /// <summary>
        /// Used to create a spreadshhet that can be returned to google sheets
        /// </summary>
        /// <param name="data"></param>
        public ValueRange(List<string> data)
        {
            values.Add(data);
        }

        public ValueRange(string data)
        {
            values.Add(new List<string>() { data });
        }

        public void Add(List<string> data)
        {
            values.Add(data);
        }
    }
}