﻿namespace DelegatesHandling
{
    public interface ITypedVoidEventExecutor<TEventData>
        where TEventData : IEventMessage
    {
        void Execute(TEventData data);
    }
}