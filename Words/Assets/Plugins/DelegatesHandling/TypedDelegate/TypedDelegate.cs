﻿namespace DelegatesHandling
{
    public delegate TEventResult TypedDelegate<TEventResult, TEventData>(TEventData data)
        where TEventResult : IEventResult
        where TEventData : IEventMessage;
}