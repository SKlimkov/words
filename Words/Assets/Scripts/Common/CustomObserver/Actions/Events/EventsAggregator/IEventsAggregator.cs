﻿using CustomFactory;
using System;
using System.Collections.Generic;

namespace CustomListener
{
    public interface IEventsAggregator : IDeinitializeable
    {
        IEvent GetEventByParamType(Type type);
        IEnumerable<IEvent> EnumerateEvents();
    }
}