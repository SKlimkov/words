﻿using CustomListener;
namespace GameSystems.EventSystem
{
    public class SystemEventFacade : PublisherFacade<SystemEventFacade.IEventParam>
    {
        #region EVENTS

        public interface IEventParam : IEventParamBase { }

        public sealed class OnGameSessionNeedToStart : EventBase<OnGameSessionNeedToStart.Param>
        {
            public OnGameSessionNeedToStart(IEventIdResolver eventIdResolver) : base(eventIdResolver) { }

            public struct Param : IEventParam
            {
                /*public Param(bool isSuccess)
                {
                    this.isSuccess = isSuccess;
                }
                public bool isSuccess;*/
            }

            public override bool IsNeedToUnlistenOnTrigger { get { return false; } }

            public class Factory : IEventFactory<OnGameSessionNeedToStart>
            {
                public EventBase CreateInstance(IEventIdResolver idResolver)
                {
                    return new OnGameSessionNeedToStart(idResolver);
                }
            }
        }

        #endregion
    }
}