﻿using System.Collections.Generic;
using UnityEngine;

public static class LinAlExtensions
{
    #region LINEAR_ALGEBRA

    private static List<Vector2> v2Directions;    

    static LinAlExtensions()
    {
        v2Directions = new List<Vector2>()
        {
            Vector2.up,
            Vector2.up + Vector2.right,
            Vector2.right,
            Vector2.right + Vector2.down,
            Vector2.down,
            Vector2.down + Vector2.left,
            Vector2.left,
            Vector2.left + Vector2.up
        };        
    }

    public static IEnumerable<Vector2> EnumerateV2Directions(this Vector2 dot)
    {
        foreach (var entry in v2Directions)
        {
            Vector2 direction = dot + entry;
            yield return direction;
        }
    }   

    public static Vector2 GetProperDirection(this Vector2 root, Vector2 target)
    {
        Vector2 targetDirection = target - root;

        float minResidue;
        Vector2 mostProperDirection;
        var tsrgetDirectionNormalized = targetDirection.normalized;

        mostProperDirection = v2Directions[0];
        minResidue = 1 - Vector2.Dot(mostProperDirection, tsrgetDirectionNormalized);

        for(var i = 0; i < v2Directions.Count; i++)
        {
            var entry = v2Directions[i];
            Vector2 entryDirection = (root + entry) - root;
            float dotProduct = Vector2.Dot(entryDirection.normalized, tsrgetDirectionNormalized);
            float residue = Mathf.Abs(1 - dotProduct);

            if (residue < minResidue)
            {
                mostProperDirection = entry;
                minResidue = residue;
            }
        }

        return mostProperDirection;
    }

    private static int GetSignFactor(float value)
    {
        int sign = 1;

        if (value < 0)
        {
            sign = -1;
        }

        return sign;
    }
    
    private static int GetVector2HashCode(Vector2 origin)
    {
        int a = 63689;
        int hash = 0;        

        unchecked
        {
            hash = origin.x.GetHashCode() * a * GetSignFactor(origin.x) + origin.y.GetHashCode() * GetSignFactor(origin.y);
        }
        //Debug.LogFormat("GetVector2HashCode {0}, {1}", hash, origin);
        return hash;
    }  

    #endregion    

    #region SIMPLE

    public static float Sqr(this float value)
    {
        return (value * value);
    }

    public static int Sqr(this int value)
    {
        return (value * value);
    }

    #endregion
}
