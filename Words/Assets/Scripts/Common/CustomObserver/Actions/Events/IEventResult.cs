﻿namespace CustomListener
{
    /// <summary>
    /// This interface must be implemented by all types that event listeners will return.
    /// </summary>
    public interface IEventResult
    {

    }
}