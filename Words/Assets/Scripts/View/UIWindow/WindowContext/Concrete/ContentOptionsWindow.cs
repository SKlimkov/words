﻿using AssetsSystem;
using CustomListener;
using CustomUi;
using System.Collections.Generic;
using UnityEngine.UI;
using Zenject;
using ZenjectServices;

public class ContentOptionsWindow : WindowContextBase<ContentOptionsWindow.ShowData, ContentOptionsWindow.HideData>
{
    #region INITIALIZATION

    [Inject]
    private void ResolveDependencies(DiContainer container)
    {
        m_AssetManager = container.Resolve<IAssetManager>();
        m_WorkSheetViewFactory = container.Resolve<WorkSheetView.Factory>();
        m_UiEventsFasade = container.Resolve<PublisherFacade<UiEventFacade.IEventParam>>();
    }

    protected override void Awake()
    {
        base.Awake();

        m_ConfirmButton = GetComponentInChildren<Button>();
        m_ConfirmButton.onClick.AddListener(OnConfirmButtonClick);
    }

    #endregion

    #region CUSTOM

    private IAssetManager m_AssetManager;
    private WorkSheetView.Factory m_WorkSheetViewFactory;

    private List<WorkSheetView> m_WorkSheetList = new List<WorkSheetView>();

    public void OnShow()
    {
        var assetDecorator = m_AssetManager.GetDecoratorForFile(AssetUtility.SettingAsset.SheetId);
        var assetStorage = assetDecorator.AssetStorage;
        var root = GetComponentInChildren<ContentSelectionViewRoot>();
        var sheets = assetStorage.Enumerate();

        foreach (var entry in sheets)
        {
            var monoConfig = new MonoConfig(root.transform);
            var view = m_WorkSheetViewFactory.Create(new WorkSheetView.Config(monoConfig, entry));
            m_WorkSheetList.Add(view);
        }
    }

    private void OnHide()
    {
        foreach(var entry in m_WorkSheetList)
        {
            entry.SafeDestroy();
        }

        m_WorkSheetList.Clear();
    }

    private Button m_ConfirmButton;
    private PublisherFacade<UiEventFacade.IEventParam> m_UiEventsFasade;

    private void OnConfirmButtonClick()
    {
        m_AssetManager.UpdateStoredAsset(AssetUtility.SettingAsset.SheetId);

        m_UiEventsFasade.TriggerEvent(new UiEventFacade.ShowWindowEvent.Param("MainMenuWindow", new MainMenuWindow.ShowData()));
        m_UiEventsFasade.TriggerEvent(new UiEventFacade.HideWindowEvent.Param("ContentOptionsWindow", new ContentOptionsWindow.HideData()));
    }

    #endregion

    #region WINDOW

    protected override void OnHidePrepare(HideData data)
    {
        OnHide();
    }

    protected override void OnShowPrepare(ShowData data)
    {
        OnShow();
    }

    #endregion

    #region NESTED_TYPES

    public struct ShowData : IShowWindowData
    {
        public bool IsNeedToClear { get { return true; } }
    }

    public struct HideData : IHideWindowData
    {
        public bool IsNeedToClear { get { return true; } }
    }

    #endregion
}