﻿using CustomFactory;

namespace CustomListener
{
    /// <summary>
    /// Generalized abstraction class for all events. All inherites must inherit only across EventBase<TParam> for preventing problem with event aggregator.
    /// </summary>
    public abstract class EventBase : IEvent, IDeinitializeable
    {
        public virtual void Deinitialize() { }

        public EventBase(IEventIdResolver eventIdResolver)
        {
            EventId = eventIdResolver.ResolveId(GetType());
        }

        public int EventId { get; private set; }

        public abstract IEventTrigger Trigger { get; protected set; }
        public abstract IListenersAggregator ListenersAggregator { get; protected set; }
        
        public void Execute(IEventParamBase param)
        {
            Trigger.Execute(param);
        }

        public abstract bool IsNeedToUnlistenOnTrigger { get; }
    }

    public abstract class EventBase<TParam> : EventBase where TParam : IEventParamBase
    {
        public override void Deinitialize()
        {
            base.Deinitialize();
            (ListenersAggregator as ListenersAggregator<TParam>).Deinitialize();
            (Trigger as EventTrigger<TParam>).Deinitialize();
        }

        public EventBase(IEventIdResolver eventIdResolver) : base(eventIdResolver)
        {
            Trigger = new EventTrigger<TParam>();

            var listenerAggregator = new ListenersAggregator<TParam>();
            ListenersAggregator = listenerAggregator;
            listenerAggregator.Configurate(new ListenersAggregator<TParam>.Config() { isNeedToClear = IsNeedToUnlistenOnTrigger });
            (Trigger as EventTrigger<TParam>).Configurate(new EventTrigger<TParam>.Config() { listenersHolder = listenerAggregator });
        }

        public override sealed IEventTrigger Trigger { get; protected set; }
        public sealed override IListenersAggregator ListenersAggregator { get; protected set; }
    }
}