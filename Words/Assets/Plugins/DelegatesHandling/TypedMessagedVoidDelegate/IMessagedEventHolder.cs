﻿namespace DelegatesHandling
{
    public interface IMessagedEventHolder<THandler, TEventData>
    {
        void AddListener(THandler handler, TEventData data);
        void RemoveListener(THandler handler);
        void ClearListeners();
    }
}