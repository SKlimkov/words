﻿using UnityEngine;

namespace ZenjectServices
{
    public struct MonoConfig
    {
        public Transform root;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 localScale;

        public bool hasParent { get { return root != null; } }

        public static MonoConfig Default { get { return new MonoConfig(null, Vector3.zero, Quaternion.identity, Vector3.one); } }

        public MonoConfig(MonoConfig config)
        {
            root = config.root;
            position = config.position;
            rotation = config.rotation;
            localScale = config.localScale;
        }

        public MonoConfig(Transform root)
        {
            this.root = root;

            this.position = Vector3.zero;
            this.rotation = Quaternion.identity;
            this.localScale = Vector3.one;
        }

        public MonoConfig(Transform root, Vector3 localPosition)
        {
            this.root = root;

            this.position = localPosition;
            this.rotation = Quaternion.identity;
            this.localScale = Vector3.one;
        }

        public MonoConfig(Transform root, Vector3 localPosition, Quaternion rotation, Vector3 localScale)
        {
            this.root = root;

            this.position = localPosition;
            this.rotation = rotation;
            this.localScale = localScale;
        }        
    }
}