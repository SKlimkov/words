﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace AssetsSystem
{
    public class PlatformDependencyResolver
    {
        private readonly string m_EditorWindows;
        private readonly string m_StandAlonePathPrefix;
        private readonly string m_AndroidPathPrefix;
        private readonly string m_IOSPathPrefix;

        private readonly Dictionary<RuntimePlatform, string> m_PathPrefixDict;

        private RuntimePlatform m_CurrentPlatform;

        public PlatformDependencyResolver()
        {
            /*m_StandAlonePathPrefix = Application.streamingAssetsPath.ToString();
            m_AndroidPathPrefix = string.Format("{0}{1}{2}", "jar:file://", Application.dataPath, "!/assets");
            m_IOSPathPrefix = string.Format("{0}/Raw", Application.dataPath);*/

            m_EditorWindows = Application.streamingAssetsPath;
            m_StandAlonePathPrefix = Application.persistentDataPath;
            m_AndroidPathPrefix = Application.persistentDataPath;
            m_IOSPathPrefix = Application.persistentDataPath;

            m_PathPrefixDict = new Dictionary<RuntimePlatform, string>();
            m_PathPrefixDict.Add(RuntimePlatform.WindowsPlayer, m_StandAlonePathPrefix);
            m_PathPrefixDict.Add(RuntimePlatform.WindowsEditor, m_EditorWindows);
            m_PathPrefixDict.Add(RuntimePlatform.Android, m_AndroidPathPrefix);
            m_PathPrefixDict.Add(RuntimePlatform.IPhonePlayer, m_IOSPathPrefix);

            m_CurrentPlatform = GetPlatform();

            var msg = string.Format("Current platform detected as {0}", m_CurrentPlatform);

            //Debug.Log(msg);
            DebugHelpers.LogSimpleMessage(msg);
        }

        public string GetFileStoragePath(string path)
        {
            var result = string.Format("{0}/{1}", m_PathPrefixDict[m_CurrentPlatform], path);
            return result;
        }

        public RuntimePlatform GetPlatform()
        {
            return Application.platform;
        }
    }

    public interface ILoadableAsset<TAssetConfig>
    {
        void Create(TAssetConfig config);
        TAssetConfig GetConfig();
    }

    public interface IXmlAssetConfig
    {
        string FolderName { get; }
        string FileName { get; }
        string FileFormat { get; }
    }

    public class AssetLoader<TAsset, TConfig>
        where TAsset : class, ILoadableAsset<TConfig>, new()
        where TConfig : IXmlAssetConfig
    {
        public AssetLoader(PlatformDependencyResolver platformDependencyResolver)
        {
            m_PlatformDependencyResolver = platformDependencyResolver;
        }

        private PlatformDependencyResolver m_PlatformDependencyResolver;
        private MonoBehaviour m_MonoInstance;

        /*public void LoadAsset(TConfig config)
        {
            var path = string.Format("/{0}/{1}.{2}", config.FolderName, config.FileName, config.FileFormat);

            //Debug.LogWarningFormat("LoadAsset {0}", path);

            RuntimePlatform platform = m_PlatformDependencyResolver.GetPlatform();

            switch (platform)
            {
                case RuntimePlatform.WindowsEditor:
                    m_MonoInstance.StartCoroutine(LoadAssetWindows(path, config.FileName));
                    break;
                case RuntimePlatform.WindowsPlayer:
                    m_MonoInstance.StartCoroutine(LoadAssetWindows(path, config.FileName));
                    break;
                case RuntimePlatform.Android:
                    m_MonoInstance.StartCoroutine(LoadAssetAndroid(path, config.FileName));
                    break;
                case RuntimePlatform.IPhonePlayer:
                    m_MonoInstance.StartCoroutine(LoadAssetWindows(path, config.FileName));
                    break;
                default:
                    m_MonoInstance.StartCoroutine(LoadAssetWindows(path, config.FileName));
                    break;
            }
        }

        private IEnumerator LoadAssetAndroid(string path, string name)
        {
            string loadingFullPath;

            string persistentPath = string.Format("{0}/{1}", Application.persistentDataPath, path);

            if (File.Exists(persistentPath))
                loadingFullPath = "file:///" + persistentPath;
            else
                loadingFullPath = m_PlatformDependencyResolver.GetStreamingAssetsPath(path);

            WWW www = new WWW(loadingFullPath);

            if (!string.IsNullOrEmpty(www.error))
                Debug.LogError("-------Can't read");

            yield return www;

            TAsset asset = GetXmlAsset(www);

            OnAssetLoadedHandler(asset);
        }

        private IEnumerator LoadAssetWindows(string path, string name)
        {
            string loadingFullPath = m_PlatformDependencyResolver.GetStreamingAssetsPath(path);

            //Debug.LogFormat("LoadAssetWindows {0}, {1}", path, name);

            TAsset asset = XmlResource.LoadFromFile<TAsset>(loadingFullPath);

            yield return null;
            OnAssetLoadedHandler(asset);
        }

        public static T LoadFromFile<T>(string path) where T : class, new()
        {
            return LoadFromFile<T>(path, true);
        }

        public static T LoadFromFile<T>(string path, bool backup) where T : class, new()
        {
            StreamReader streamReader = null;
            XmlSerializer xmlSerializer = null;
            T deserializedObject = null;

            try
            {
                streamReader = new StreamReader(path);
                xmlSerializer = new XmlSerializer(typeof(T));
                deserializedObject = xmlSerializer.Deserialize(streamReader) as T;
                streamReader.Close();
            }
            catch (System.Exception e)
            {
                Debug.LogWarning("Unable to load file at : " + path + "\n" + e.Message);

                if (streamReader != null)
                    streamReader.Close();
                if (File.Exists(path))
                    File.Delete(path);

                if (backup)
                {
                    Debug.LogWarning("Attempt to restore from backup");

                    string backupFilename = path + BACKUP_NAME_POSTFIX;
                    if (File.Exists(backupFilename))
                    {
                        File.Copy(backupFilename, path, true);
                        deserializedObject = LoadFromFile<T>(path, false);
                    }
                    else
                    {
                        Debug.LogWarning("Unable to find backup file at : " + backupFilename);
                    }
                }
            }
            finally
            {
                //
            }

            return deserializedObject;
        }

        public static bool SaveToFile(string path, object data)
        {
            return SaveToFile(path, data, true);
        }

        public static bool SaveToFile(string path, object data, bool backup)
        {
            bool saved = false;

            if (backup && File.Exists(path))
            {
                try
                {
                    File.Copy(path, path + BACKUP_NAME_POSTFIX, true);
                }
                catch (System.Exception e)
                {
                    Debug.Log("Unable to create backup file at : " + path + "\n" + e.Message);
                }
            }

            StreamWriter streamWriter = null;
            XmlSerializer xmlSerializer = null;

            try
            {
                streamWriter = new StreamWriter(path, false);
                xmlSerializer = new XmlSerializer(data.GetType());
                xmlSerializer.Serialize(streamWriter, data);
                streamWriter.Flush();
                streamWriter.Close();

                saved = true;
                //Debug.Log("Saved at: " + path);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Unable to save file at : " + path + "\n" + e.Message);
                if (streamWriter != null)
                    streamWriter.Close();
            }
            finally
            {
                //
            }

            return saved;
        }*/
    }
}