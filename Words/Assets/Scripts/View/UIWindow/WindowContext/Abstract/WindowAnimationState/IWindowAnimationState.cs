﻿public enum EWindowAnimationState
{
    Showing,
    Hiding,
    Normal
};

public interface IWindowAnimationState
{
    void OnAnimationBreak(IWindowAnimationContext context);

    EWindowAnimationState Type { get; }
}