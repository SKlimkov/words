﻿namespace CustomActions
{
    public interface IGenericVoidExecutable<TParam>
        where TParam : IExecutableParam
    {
        void Execute(TParam param);
    }
}