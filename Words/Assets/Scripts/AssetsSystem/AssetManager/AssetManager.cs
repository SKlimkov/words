﻿using RuntimeSerialization;
using System;
using System.Collections.Generic;
using System.IO;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;

namespace AssetsSystem
{
    public interface IAssetManager : IAssetDecoratorManager
    {
        bool CheckAssetForFile(string fileId);
        UniTask<FileAsset> LoadAssetFile(string fileId);        
        void RewriteFile(string fileId, FileAsset asset);
        void UpdateStoredAsset(string fileId);
    }

    public interface IAssetDecoratorManager
    {
        IFileAssetDecorator GetDecoratorForFile(string fileId);
        void CreateDecorator(FileAsset asset);
    }

    public class AssetManager : IAssetManager
    {
        [Inject]
        private void ResolveDependencies(DiContainer container)
        {
            m_PlatformDependenciesResolver = container.Resolve<PlatformDependencyResolver>();
            m_Serializator = container.Resolve<ISerializator<byte[]>>();
        }

        private PlatformDependencyResolver m_PlatformDependenciesResolver;
        public bool CheckAssetForFile(string fileId)
        {
            var path = GetPath(fileId, true);
            return File.Exists(path);           
        }

        private string GetPath(string fileName, bool needToCreateFolder = false)
        {
            var folderPath = m_PlatformDependenciesResolver.GetFileStoragePath(folderName);
            if (needToCreateFolder)
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
            }

            //Debug.LogFormat("GetPath {0}, {1}", folderPath, folderName);
            return string.Format("{0}/{1}.{2}", folderPath, fileName, fileFormat);            
        }

        private readonly string folderName = "FileAssets";
        private readonly string fileFormat = "fileAsset";

        ISerializator<byte[]> m_Serializator;

        public async UniTask<FileAsset> LoadAssetFile(string fileId)
        {
            var path = GetPath(fileId);
            
            var request = new UnityWebRequest(path);
            DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
            request.downloadHandler = dH;

            Debug.LogFormat("LoadAssetFile {0}, {1}", Time.time, path);

            var requestResult = await request.SendWebRequest();

            if (!requestResult.isDone || requestResult.isNetworkError)
            {
                var error = string.Format("{0}", requestResult.error);
                Debug.LogErrorFormat(error);
                throw new Exception(error);
            }

            Debug.LogFormat("{0}, {1}, {2}", Time.time, requestResult.isDone, requestResult.isNetworkError);
            var file = requestResult.downloadHandler.data;
            var asset = m_Serializator.DeserializeObject<FileAsset>(file);
            return asset;
        }

        private Dictionary<string, IFileAssetDecorator> m_DecoratorDict = new Dictionary<string, IFileAssetDecorator>();

        public IFileAssetDecorator GetDecoratorForFile(string fileId)
        {
            IFileAssetDecorator result;

            var success = m_DecoratorDict.TryGetValue(fileId, out result);

            if (success)
            {
                return result;
            }
            else
            {
                throw new System.Exception(string.Format("Can't find decorator for {0} file", fileId));
            }            
        }

        public void CreateDecorator(FileAsset asset)
        {
            var decorator = new FileAssetDecorator(asset);
            var id = asset.FileId;

            if (!m_DecoratorDict.ContainsKey(id))
            {
                m_DecoratorDict.Add(id, decorator);
            }
            else
            {
                throw new System.Exception("Decorator for {0} file already exists!");
            }
        }

        public void UpdateStoredAsset(string fileId)
        {
            var decorator = GetDecoratorForFile(fileId) as FileAssetDecorator;

            RewriteFile(fileId, decorator.FileAssetinstance);
        }

        public void RewriteFile(string fileId, FileAsset asset)
        {
            //Debug.LogFormat("RewriteFile A {0}", asset);

            var file = m_Serializator.SerializeObject(asset);

            //Debug.LogFormat("RewriteFile B {0}", file);
            File.WriteAllBytes(GetPath(fileId), file);
        }        
    }    
}