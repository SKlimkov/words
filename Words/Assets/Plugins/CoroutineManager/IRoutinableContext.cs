﻿using UnityEngine;

namespace CoroutineHandling
{
    public interface IRoutinableContext
    {
        void OnStart();
        bool OnConditionCheking();
        void OnAction();
        void OnFinish();

        MonoBehaviour MonoInstance { get; }
    }
}