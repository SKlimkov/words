﻿using CustomFactory;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using ZenjectServices;

namespace CustomListener
{
    /// <summary>
    /// Delegate for all event listeners
    /// </summary>
    /// <typeparam name="TParam">Event listener parameter type. Must implement IEventParam.</typeparam>
    /// <param name="param">Event listener parameter value.</param>
    public delegate void OnEventTriggerHandler<TParam>(TParam param) where TParam : IEventParamBase;

    /// <summary>
    /// Container for holding all delegates that listen event.
    /// </summary>
    /// <typeparam name="TParam">Event listener parameter type. Must implement IEventParam.</typeparam>
    public class ListenersAggregator<TParam> : IListenersAggregator, IEventTrigger, IConfigurateable<ListenersAggregator<TParam>.Config>, IDeinitializeable
        where TParam : IEventParamBase
    {
        public virtual void Configurate(Config config)
        {
            IsNeedToClear = config.isNeedToClear;
        }

        public virtual void Deinitialize()
        {
            m_Listeners.Clear();
        }    

        /// <summary>
        /// Dictionary for storing and finding listeners (delegates) on custom events. Hash code of delegates used as key of dictionary.
        /// </summary>
        private Dictionary<int, OnEventTriggerHandler<TParam>> m_Listeners;

        public ListenersAggregator()
        {
            m_Listeners = new Dictionary<int, OnEventTriggerHandler<TParam>>();
        }

        #region TRIGGER

        void IEventTrigger.Execute(IEventParamBase param)
        {
            if (param.GetType() == typeof(TParam))
            {
                Execute((TParam)param);
            }
            else
            {
                throw new System.Exception(string.Format("Wrong argumet type. Expected type is {0}, but received type is {1}", typeof(TParam), param.GetType()));
            }
        }

        public void Execute(TParam param)
        {
            foreach (var entry in m_Listeners)
            {
                entry.Value(param);
            }

            if (IsNeedToClear)
            {
                m_Listeners.Clear();
            }
        }

        public bool IsNeedToClear { get; private set; }

        #endregion

        #region LISTENERS_HOLDER

        public void AddListener<KParam>(OnEventTriggerHandler<KParam> listener) where KParam : IEventParamBase
        {
            if (typeof(KParam) == typeof(TParam))
            {
                AddListenerInternal(listener as OnEventTriggerHandler<TParam>);
            }
            else
            {
                throw new System.Exception(
                    string.Format("Wrong delegate parameter type. The type is {0}, but expected type is {1}", typeof(KParam), typeof(TParam)));
            }
        }

        private bool isSomeOneListen;
        private int listenerHash;

        private void AddListenerInternal(OnEventTriggerHandler<TParam> listener)
        {
            var key = listener.GetHashCode();

            if (!isSomeOneListen)
            {
                isSomeOneListen = true;
                listenerHash = key;
            }

            //Debug.LogFormat("AddListenerInternal param: {0}, listenerHash: {1}, aggregatorHash: {2}", typeof(TParam), key, GetHashCode());

            if (m_Listeners.ContainsKey(key))
            {
                UnityEngine.Debug.LogWarningFormat("Listener with key {0} and type {1} is already exits in dictionary ", key, listener.GetType());
            }

            m_Listeners[key] = listener;
        }

        public void RemoveListener<KParam>(OnEventTriggerHandler<KParam> listener) where KParam : IEventParamBase
        {
            if (typeof(KParam) == typeof(TParam))
            {
                RemoveListenerInternal(listener as OnEventTriggerHandler<TParam>);
            }
            else
            {
                throw new System.Exception(string.Format("Wrong delegate parameter type. The type is {0}, but expected type is {1}", typeof(KParam), typeof(TParam)));
            }
        }

        private void RemoveListenerInternal(OnEventTriggerHandler<TParam> listener)
        {
            var key = listener.GetHashCode();

            //Debug.LogFormat("RemoveListener param: {0}, listenerHash: {1}, aggregatorHash: {2}", typeof(TParam), key, GetHashCode());

            if (!m_Listeners.ContainsKey(key))
            {
                Debug.LogFormat("{0}, {1}", isSomeOneListen, listenerHash);
                throw new System.Exception(string.Format("Can not find listener with key {0} and type {1} in dictionary", key, listener.GetType()));
            }

            m_Listeners.Remove(key);
        }

        public void ClearAllListeners()
        {
            m_Listeners.Clear();
        }

        #endregion

        #region NESTED_TYPES

        public struct Config 
        {
            public bool isNeedToClear;
        }

        public class Factory : PlaceHolderFactoryWrapper<Config, ListenersAggregator<TParam>>
        {
            public Factory(DiContainer container) : base(container) { }

            protected override ListenersAggregator<TParam> CreateNewInstance(ListenersAggregator<TParam>.Config config)
            {
                return new ListenersAggregator<TParam>();
            }
        }

        #endregion
    }
}