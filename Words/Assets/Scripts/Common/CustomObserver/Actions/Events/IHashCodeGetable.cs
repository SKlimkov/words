﻿namespace CustomListener
{
    /// <summary>
    /// Interface for another abstraction that can't return standart object GetHashCode().
    /// </summary>
    public interface IHashCodeGetable
    {
        /// <summary>
        /// GetHashCode signature.
        /// </summary>
        /// <returns>Returned hash code</returns>
        int GetHashCode();
    }
}