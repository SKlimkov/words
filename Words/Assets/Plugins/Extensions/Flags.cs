﻿using System;
using System.Collections.Generic;

public static class Flags
{

    public static bool IsFlagsContains<T>(this T conditions, T value) where T : struct, IConvertible
    {
        throw new Exception(string.Format("This usage is obsolete! For preventing unnecessary boxing use instead IsBitmaskContains"));

        /*int optionsValue = (int)(object)conditions;
        int conditionValueValue = (int)(object)value;

        return (optionsValue & conditionValueValue) == conditionValueValue;*/
    }

    public static bool IsBitmaskContains(this int conditions, int value)
    {
        return (conditions & value) == value;
    }
    
    public static int ToEnum<TEnum>(this List<TEnum> list) where TEnum: struct, IConvertible
    {
        int result = 0;

        Type type = typeof(TEnum);
        if (type.IsEnum)
        {
            foreach (var entry in list)
            {
                result += Convert.ToInt32(entry);
            }
        }

        return result;
    }

    public static List<int> ToIntList<TEnum>(this int value) where TEnum : struct, IConvertible
    {
        List<int> list = new List<int>();

        var values = Enum.GetValues(typeof(TEnum)) as int[];

        //UnityEngine.Debug.LogErrorFormat("ToIntList {0}, {1}", values, values.Length);

        for(var i = 0; i < values.Length; i++)
        {
            var intEntry = values[i];
            if (value.IsBitmaskContains(intEntry))
            {
                list.Add(intEntry);
            }
        }

        /*foreach (var entry in values)
        {
            var intEntry = (int)entry;
            if (value.IsBitmaskContains(intEntry))
            {
                list.Add(intEntry);
            }
        }*/

        return list;
    }

    public static List<int> ToIntList<TEnum>(this int value, List<int> enumValuesList) where TEnum : struct, IConvertible
    {
        List<int> list = new List<int>(enumValuesList.Count);

        for(var i = 0; i < enumValuesList.Count; i++)
        {
            var intEntry = enumValuesList[i];
            if (value.IsBitmaskContains(intEntry))
            {
                list.Add(intEntry);
            }
        }

        return list;
    }

    public static List<TEnum> ToEnumList<TEnum>(this int value) where TEnum : struct, IConvertible
    {
        List<TEnum> list = new List<TEnum>();

        var values = Enum.GetValues(typeof(TEnum));

        foreach (var entry in values)
        {
            var intEntry = (int)entry;
            if (value.IsBitmaskContains(intEntry))
            {
                list.Add((TEnum)entry);
            }
        }

        return list;
    }


    /*public static bool IsSet<T>(T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        return (flagsValue & flagValue) != 0;
    }

    public static void Set<T>(ref T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        flags = (T)(object)(flagsValue | flagValue);
    }

    public static void Unset<T>(ref T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        flags = (T)(object)(flagsValue & (~flagValue));
    }*/
}
