﻿namespace CustomFactory
{
    public interface IConfigurateable<TConfig>
    {
        void Configurate(TConfig config);
    } 
}
