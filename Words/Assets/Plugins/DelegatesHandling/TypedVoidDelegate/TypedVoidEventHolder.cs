﻿namespace DelegatesHandling
{
    public delegate void TypedVoidDelegate<TEventData>(TEventData data)
        where TEventData : IEventMessage;

    public class TypedVoidEventHolder<TEventData> : IEventHolder<TypedVoidDelegate<TEventData>>, ITypedVoidEventExecutor<TEventData>
        where TEventData : IEventMessage
    {
        private TypedVoidDelegate<TEventData> onEventHandler;        

        public void AddListener(TypedVoidDelegate<TEventData> handler)
        {
            /*if (typeof(TEventData) == typeof(StatusEffectMessage))
            {
                UnityEngine.Debug.LogFormat("AddListener {0}, {1}", handler.Method.Name, this.GetHashCode());
            } */           
            onEventHandler += handler;
        }

        public void RemoveListener(TypedVoidDelegate<TEventData> handler)
        {
            //bool isValid = onEventHandler.GetInvocationList().
            if (onEventHandler != null)
            {
                onEventHandler -= handler;
            }
        }

        public void ClearListeners()
        {
            onEventHandler = null;
        }

        public void Execute(TEventData data)
        {
            if (onEventHandler != null)
            {
                onEventHandler(data);

                if (data.IsNeedToClear)
                {
                    ClearListeners();
                }
            }
        }

        public override string ToString()
        {
            var invocationList = onEventHandler.GetInvocationList();

            string list = "";

            foreach(var entry in invocationList)
            {
                var methodInfo = entry.Method;
                list = string.Format("{0} [{1}, {2}];", list, methodInfo.Name, methodInfo.ReflectedType);
            }
            return base.ToString();
        }
    }
}