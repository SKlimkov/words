﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using GoogleSheetsToUnity.ThirdPary;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using TinyJSON;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Linq;
using System;
using UniRx.Async;

namespace GoogleSheetsToUnity
{
    public class SpreadsheetAsynkManager
    {
        #region ASYNC

        public static async UniTask<GoogleAsyncAuthrisationHelper.Result> RefreshToken()
        {
            if (Application.isPlaying)
            {
                try
                {
                    var result = await GoogleAsyncAuthrisationHelper.RefreshToken();
                    //Debug.LogFormat("RefreshToken {0}", result.isSuccess);
                    return result;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                throw new Exception(string.Format("You try to refresh token in an edit mode, but it is prohibited"));
            }
        }

        public struct SheetListInfo
        {
            public List<Properties> sheets;
            public bool isSuccess;
        }

        public static async UniTask<SheetListInfo> EnumerateWorkSheets(GSTU_Search search, bool containsMergedCells = false)
        {
            //Debug.LogFormat("EnumerateWorkSheets");

            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + search.sheetId);
            sb.Append("?access_token=" + SpreadsheetManager.Config.gdr.access_token);

            var request = UnityWebRequest.Get(sb.ToString());

            try
            {
                var result = await ReadWorkSheets(request, search, containsMergedCells);

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static async UniTask<SheetListInfo> ReadWorkSheets(UnityWebRequest request, GSTU_Search search, bool containsMergedCells)
        {
            try
            {
                await request.SendWebRequest();

                bool isSuccess = false;

                if (string.IsNullOrEmpty(request.downloadHandler.text) || request.downloadHandler.text == "{}")
                {
                    Debug.LogWarning("Unable to Retreive data from google sheets");
                }
                else if (request.isHttpError || request.isNetworkError)
                {
                    Debug.LogErrorFormat("Error: {0}", request.error);
                    Debug.LogErrorFormat("Error: {0}", request.downloadHandler.text);
                }
                else
                {
                    isSuccess = true;
                }

                var result = new SheetListInfo() { isSuccess = isSuccess, sheets = new List<Properties>() };

                if (string.IsNullOrEmpty(request.downloadHandler.text) || request.downloadHandler.text == "{}")
                {
                    Debug.Log("Unable to Retreive data from google sheets");
                    result.isSuccess = false;
                }
                else
                {
                    var root = JsonUtility.FromJson<SheetsRootObject>(request.downloadHandler.text);
                    foreach (var enty in root.sheets)
                    {
                        result.sheets.Add(enty.properties);
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public struct SpreadSheetInfo
        {
            public bool isSuccess;
            public GstuSpreadSheet sheet;
        }

        public static async UniTask<SpreadSheetInfo> EditorRead(GSTU_Search search, Properties properties, bool containsMergedCells = false)
        {
            try
            {
                return await ReadWithoutCheck(GetRequest(search), search, properties, containsMergedCells);
            }
            catch (Exception e)
            {
                throw e;
            }
        }        

        private static void SaveFile(string content)
        {
            string folderName = Application.persistentDataPath;
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                folderName = Application.dataPath + "/StreamingAssets";
            }

            var fileName = "receivedtext";
            var fileFormat = "txt";
            var path = string.Format("{0}/{1}.{2}", folderName, fileName, fileFormat);

            Debug.LogFormat("{0}", path);

            if (!System.IO.Directory.Exists(folderName))
            {
                System.IO.Directory.CreateDirectory(folderName);
            }

            //var path = "c:\temp\file.txt";
            System.IO.File.WriteAllText(path, content, Encoding.Default);

            Debug.LogFormat("Saved!");            
        }

        static async UniTask<SpreadSheetInfo> ReadWithoutCheck(UnityWebRequest request, GSTU_Search search, Properties properties, bool containsMergedCells)
        {
            try
            {
                await request.SendWebRequest();

                SpreadSheetInfo result = new SpreadSheetInfo();

                if (string.IsNullOrEmpty(request.downloadHandler.text) || request.downloadHandler.text == "{}")
                {
                    Debug.LogWarning("Unable to Retreive data from google sheets");
                }
                else if (request.isHttpError || request.isNetworkError)
                {
                    Debug.LogErrorFormat("Error: {0}", request.error);
                    Debug.LogErrorFormat("Error: {0}", request.downloadHandler.text);
                }
                else
                {
                    result.isSuccess = true;
                }

                //ValueRange rawData = JSON.Load(request.downloadHandler.text).Make<ValueRange>();

                var json = SimpleJSON.JSON.Parse(request.downloadHandler.text);
                var array = json["values"].AsArray;

                List<List<string>> values = new List<List<string>>();

                foreach(var entry in array)
                {
                    var list = new List<string>();

                    //Debug.LogFormat("Next");

                    foreach (var subEntry in entry.Value.AsArray)
                    {
                        var value = subEntry.Value.Value;
                        list.Add(value);
                        //Debug.LogFormat("value {0}", value);
                    }

                    values.Add(list);
                }

                //Debug.LogFormat("--------------------- {0}, {1}", array.Count, values.Count);

                var rawData = JsonUtility.FromJson<ValueRange>(request.downloadHandler.text);
                rawData.values = values;
                /*Debug.LogErrorFormat("{0}", rawData.values.Count);

                foreach (var entry in rawData.values)
                {
                    Debug.LogFormat("{0}", entry);
                } */               

                //SaveFile(request.downloadHandler.text);

                //Debug.LogFormat("{0}, {1}", containsMergedCells, rawData.range);
                //Debug.LogFormat("{0}", request.downloadHandler.text);

                GSTU_SpreadsheetResponce responce = new GSTU_SpreadsheetResponce(rawData);

                //if it contains merged cells then process a second set of json data to know what these cells are
                if (containsMergedCells)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
                    sb.Append("/" + search.sheetId);
                    sb.Append("?access_token=" + SpreadsheetManager.Config.gdr.access_token);

                    UnityWebRequest request2 = UnityWebRequest.Get(sb.ToString());

                    await request2.SendWebRequest();

                    //SheetsRootObject root = JSON.Load(request2.downloadHandler.text).Make<SheetsRootObject>();
                    var root = JsonUtility.FromJson<SheetsRootObject>(request2.downloadHandler.text);
                    responce.sheetInfo = root.sheets.FirstOrDefault(x => x.properties.title == search.worksheetName);
                }

                result.sheet = new GstuSpreadSheet(responce, search.titleColumn, search.titleRow, search.worksheetName);

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static UnityWebRequest GetRequest(GSTU_Search search)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + search.sheetId);
            sb.Append("/values");
            sb.Append("/" + search.worksheetName + "!" + search.startCell + ":" + search.endCell);
            sb.Append("?access_token=" + SpreadsheetManager.Config.gdr.access_token);

            UnityWebRequest request = UnityWebRequest.Get(sb.ToString());

            return request;
        }

        #endregion
    }

    /// <summary>
    /// Partial class for the spreadsheet manager to handle all private functions
    /// </summary>
    public partial class SpreadsheetManager
    {
        /// <summary>
        /// Метод, аналогичный соседнему, но включает в себя замену вебреквеста на актуальный,
        /// что ведет к исправлению ошибки
        /// </summary>
        /// <param name="action"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        static IEnumerator CheckForRefreshToken(Action<UnityWebRequest> action, GSTU_Search search)
        {
            Debug.LogFormat("CheckForRefreshToken 1");
            Action<bool, Action<bool, bool>> refreshCheckAction = (isNeedToGetNewrequest, onComplete) =>
            {
                onComplete(isNeedToGetNewrequest, true);
                //Debug.LogFormat("CheckNetwork {0}", isConnected);
                if (isNeedToGetNewrequest)
                {
                    action(GetRequest(search));
                }
                else
                {
                    action(null);
                }
            };

            Action<bool, bool> onCompleteAction = (isSuccess, isAssetForFile) =>
            {
                Debug.LogFormat("{0}, {1}", isSuccess, isAssetForFile);
            };

            if (Application.isPlaying)
            {
                yield return new Task(GoogleAuthrisationHelper.CheckForRefreshOfToken(refreshCheckAction, onCompleteAction));
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(GoogleAuthrisationHelper.CheckForRefreshOfToken(refreshCheckAction, onCompleteAction));
            }
#endif
        }        

        /// <summary>
        /// Chekcs for a valid token and if its out of date attempt to refresh it
        /// </summary>
        /// <returns></returns>
        public static IEnumerator CheckForRefreshToken(Action<bool, Action<bool, bool>> completionHandler, Action<bool, bool> onComplete)
        {
            if (Application.isPlaying)
            {
                yield return new Task(GoogleAuthrisationHelper.CheckForRefreshOfToken(completionHandler, onComplete));
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(GoogleAuthrisationHelper.CheckForRefreshOfToken(completionHandler, onComplete));
            }
#endif
        }

        public static IEnumerator CheckForRefreshToken()
        {
            Debug.LogFormat("CheckForRefreshToken 3 {0}", Application.isPlaying);
            if (Application.isPlaying)
            {
                yield return new Task(GoogleAuthrisationHelper.CheckForRefreshOfToken());
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(GoogleAuthrisationHelper.CheckForRefreshOfToken());
            }
#endif
        }

        public static IEnumerator CheckForRefreshToken(Action action)
        {
            Debug.LogFormat("CheckForRefreshToken 4 {0}", Application.isPlaying);
            if (Application.isPlaying)
            {
                yield return new Task(CheckForRefreshToken());
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(CheckForRefreshToken());
            }
#endif
        }             

        #region BASICS

        /// <summary>
        /// Reads information from a spreadsheet
        /// </summary>
        /// <param name="search"></param>
        /// <param name="callback"></param>
        /// <param name="containsMergedCells"> does the spreadsheet contain merged cells, will attempt to group these by titles</param>
        public static void UniversalRead(GSTU_Search search, UnityAction<GstuSpreadSheet> callback, bool containsMergedCells = false)
        {
            UnityWebRequest request = GetRequest(search);

            //Debug.LogFormat("Read {0}", Application.isPlaying);

            if (Application.isPlaying)
            {
                new Task(Read(request, search, containsMergedCells, callback));
            }
#if UNITY_EDITOR
            else
            {
                EditorCoroutineRunner.StartCoroutine(Read(request,  search, containsMergedCells, callback));
            }
#endif
        }

        private static UnityWebRequest GetRequest(GSTU_Search search)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + search.sheetId);
            sb.Append("/values");
            sb.Append("/" + search.worksheetName + "!" + search.startCell + ":" + search.endCell);
            sb.Append("?access_token=" + Config.gdr.access_token);

            UnityWebRequest request = UnityWebRequest.Get(sb.ToString());

            return request;
        }
        
        /// <summary>
        /// Reads the spread sheet and callback with the results
        /// </summary>
        /// <param name="request"></param>
        /// <param name="search"></param>
        /// <param name="containsMergedCells"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        static IEnumerator Read(UnityWebRequest request, GSTU_Search search, bool containsMergedCells, UnityAction<GstuSpreadSheet> callback)
        {
            Action<UnityWebRequest> refreshCheckAction = (newRequest) =>
            {
                //Debug.LogFormat("CheckNetwork {0}", isConnected);
                if (newRequest != null)
                {
                    request = newRequest;
                }
            };

            if (Application.isPlaying)
            {
                yield return new Task(CheckForRefreshToken(refreshCheckAction, search));
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(CheckForRefreshToken(refreshCheckAction, search));
            }
#endif

            yield return null;

            using (request)
            {
                yield return request.SendWebRequest();

                if(string.IsNullOrEmpty(request.downloadHandler.text) || request.downloadHandler.text == "{}")
                {
                    Debug.LogWarning("Unable to Retreive data from google sheets");
                    yield break;
                }

                if (request.isHttpError || request.isNetworkError)
                {
                    Debug.LogErrorFormat("Error: {0}", request.error);
                    Debug.LogErrorFormat("Error: {0}", request.downloadHandler.text);
                    yield break;
                }


                //ValueRange rawData = JSON.Load(request.downloadHandler.text).Make<ValueRange>();
                var rawData = JsonUtility.FromJson<ValueRange>(request.downloadHandler.text);

                //Debug.LogFormat("{0}, {1}", rawData.range, request.downloadHandler.text);

                GSTU_SpreadsheetResponce responce = new GSTU_SpreadsheetResponce(rawData);

                //if it contains merged cells then process a second set of json data to know what these cells are
                if (containsMergedCells)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
                    sb.Append("/" + search.sheetId);
                    sb.Append("?access_token=" + Config.gdr.access_token);

                    UnityWebRequest request2 = UnityWebRequest.Get(sb.ToString());

                    yield return request2.SendWebRequest();

                    //SheetsRootObject root = JSON.Load(request2.downloadHandler.text).Make<SheetsRootObject>();
                    var root = JsonUtility.FromJson<SheetsRootObject>(request2.downloadHandler.text);
                    responce.sheetInfo = root.sheets.FirstOrDefault(x => x.properties.title == search.worksheetName);
                }

                if (callback != null)
                {
                    callback(new GstuSpreadSheet(responce, search.titleColumn,search.titleRow, search.worksheetName));
                }
            }
        }        

        /// <summary>
        /// Updates just the cell pased in as the startCell value of the search parameters
        /// </summary>
        /// <param name="search"></param>
        /// <param name="inputData"></param>
        /// <param name="callback"></param>
        public static void Write(GSTU_Search search, string inputData, UnityAction callback)
        {
            Write(search, new ValueRange(inputData), callback);
        }

        /// <summary>
        /// Writes data to a spreadsheet
        /// </summary>
        /// <param name="search"></param>
        /// <param name="inputData"></param>
        /// <param name="callback"></param>
        public static void Write(GSTU_Search search, ValueRange inputData, UnityAction callback)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + search.sheetId);
            sb.Append("/values");
            sb.Append("/" + search.worksheetName + "!" + search.startCell + ":" + search.endCell);
            sb.Append("?valueInputOption=USER_ENTERED");
            sb.Append("&access_token=" + Config.gdr.access_token);

            string json = JSON.Dump(inputData, EncodeOptions.NoTypeHints);
            byte[] bodyRaw = new UTF8Encoding().GetBytes(json);

            UnityWebRequest request = UnityWebRequest.Put(sb.ToString(), bodyRaw);

            if (Application.isPlaying)
            {
                new Task(Write(request, callback));
            }
#if UNITY_EDITOR
            else
            {
                EditorCoroutineRunner.StartCoroutine(Write(request, callback));
            }
#endif
        }

        static IEnumerator Write(UnityWebRequest request, UnityAction callback)
        {
            if (Application.isPlaying)
            {
                yield return new Task(CheckForRefreshToken());
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(CheckForRefreshToken());
            }
#endif

            using (request)
            {
                yield return request.SendWebRequest();

                if (callback != null)
                {
                    callback();
                }
            }
        }

        /// <summary>
        /// Writes a batch update to a spreadsheet
        /// </summary>
        /// <param name="search"></param>
        /// <param name="requestData"></param>
        /// <param name="callback"></param>
        public static void WriteBatch(GSTU_Search search, BatchRequestBody requestData, UnityAction callback)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + search.sheetId);
            sb.Append("/values:batchUpdate");
            sb.Append("?access_token=" + Config.gdr.access_token);


            string json = JSON.Dump(requestData, EncodeOptions.NoTypeHints);
            UnityWebRequest request = UnityWebRequest.Post(sb.ToString(), "");
            byte[] bodyRaw = new UTF8Encoding().GetBytes(json);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();

            if (Application.isPlaying)
            {
                new Task(WriteBatch(request, callback));
            }
#if UNITY_EDITOR
            else
            {
                EditorCoroutineRunner.StartCoroutine(WriteBatch(request, callback));
            }
#endif
        }

        static IEnumerator WriteBatch(UnityWebRequest request, UnityAction callback)
        {
            if (Application.isPlaying)
            {
                yield return new Task(CheckForRefreshToken());
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(CheckForRefreshToken());
            }
#endif

            using (request)
            {
                yield return request.SendWebRequest();

                if (callback != null)
                {
                    callback();
                }
            }
        }

        /// <summary>
        /// Adds the data to the next avaiable space to write it after the startcell
        /// </summary>
        /// <param name="search"></param>
        /// <param name="inputData"></param>
        /// <param name="callback"></param>
        public static void Append(GSTU_Search search, ValueRange inputData, UnityAction callback)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("https://sheets.googleapis.com/v4/spreadsheets");
            sb.Append("/" + search.sheetId);
            sb.Append("/values");
            sb.Append("/" + search.worksheetName + "!" + search.startCell);
            sb.Append(":append");
            sb.Append("?valueInputOption=USER_ENTERED");
            sb.Append("&access_token=" + Config.gdr.access_token);

            string json = JSON.Dump(inputData, EncodeOptions.NoTypeHints);

            UnityWebRequest request = UnityWebRequest.Post(sb.ToString(), "");

            //have to do this cause unitywebrequest post will nto accept json data corrently...
            byte[] bodyRaw = new UTF8Encoding().GetBytes(json);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();

            if (Application.isPlaying)
            {
                new Task(Append(request, callback));
            }
#if UNITY_EDITOR
            else
            {
                EditorCoroutineRunner.StartCoroutine(Append(request, callback));
            }
#endif
        }

        static IEnumerator Append(UnityWebRequest request, UnityAction callback)
        {
            if (Application.isPlaying)
            {
                yield return new Task(CheckForRefreshToken());
            }
#if UNITY_EDITOR
            else
            {
                yield return EditorCoroutineRunner.StartCoroutine(CheckForRefreshToken());
            }
#endif

            using (request)
            {
                yield return request.SendWebRequest();

                if (callback != null)
                {
                    callback();
                }
            }
        }

        #endregion
    }
}