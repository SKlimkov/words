﻿namespace CustomFactory
{
    public interface IFactory<TConfig, TResult> where TResult : IConfigurateable<TConfig>
    {
        TResult Create(TConfig config);
    }
}