﻿using DelegatesHandling;
using UnityEngine;

public interface IWindowView
{
    void Show();
    void Hide();

    void Lock();

    void Unlock();

    IEventHolder<VoidDelegate> OnShowingUp { get; }
    IEventHolder<VoidDelegate> OnHidingUp { get; }
}