﻿using UnityEngine;

namespace CoroutineHandling
{
    public enum ELazyActionType
    {
        Null,
        EndOfFrame,
        FixedUpdate
    };

    public interface IRoutineActionsHolder
    {
        System.Collections.IEnumerator GetAction(ELazyActionType actionType);
        System.Collections.IEnumerator GetAction(float delay);
    }

    public class RoutineActionsHolder : IRoutineActionsHolder
    {
        private delegate System.Collections.IEnumerator RoutineAction();
        private System.Collections.Generic.Dictionary<int, RoutineAction> m_LazyActions;

        public RoutineActionsHolder()
        {
            m_LazyActions = new System.Collections.Generic.Dictionary<int, RoutineAction>();

            RoutineAction nullAction = NullAction;

            m_LazyActions.Add((int)ELazyActionType.Null, nullAction);
            m_LazyActions.Add((int)ELazyActionType.EndOfFrame, EndOfFrameAction);
            m_LazyActions.Add((int)ELazyActionType.FixedUpdate, FixedUpdateAction);
        }

        private System.Collections.IEnumerator NullAction()
        {
            yield return null;
        }

        private System.Collections.IEnumerator EndOfFrameAction()
        {
            yield return new WaitForEndOfFrame();
        }

        private System.Collections.IEnumerator FixedUpdateAction()
        {
            yield return new WaitForFixedUpdate();
        }

        private System.Collections.IEnumerator SecondsAction(float delay)
        {
            yield return new WaitForSeconds(delay);
        }


        public System.Collections.IEnumerator GetAction(ELazyActionType actionType)
        {
            RoutineAction routine = m_LazyActions[(int)actionType];
            yield return routine();
        }

        public System.Collections.IEnumerator GetAction(float delay)
        {
            yield return SecondsAction(delay);
        }
    }
}