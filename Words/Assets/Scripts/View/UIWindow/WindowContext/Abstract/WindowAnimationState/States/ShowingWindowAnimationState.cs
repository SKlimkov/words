﻿public class ShowingWindowAnimationState : IWindowAnimationState
{
    public void OnAnimationBreak(IWindowAnimationContext context)
    {
        context.OnShowingAnimationAborted();
    }

    public EWindowAnimationState Type { get { return EWindowAnimationState.Showing; } }
}