﻿//using DelegatesHandling;
//using OneLineNamespace;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Linq;
using CustomListener;
using CustomUi;
using DelegatesHandling;

public interface IShowWindowData : IEventMessage
{

}

public interface IHideWindowData : IEventMessage
{

}

public interface IWindowManager
{
    void RegisterWindow(string windowId, IWindowContext window);
    void UnRegisterWindow(string windowId);
}

public class WindowManager : IWindowManager
{
    #region INITIALIZATION

    public WindowManager()
    {
        m_Windows = new Dictionary<string, IWindowContext>();
    }

    [Inject]
    private void ResolveDependencies(DiContainer container)
    {
        //Debug.LogFormat("ResolveDependencies");
        m_EventsFasade = container.Resolve<PublisherFacade<UiEventFacade.IEventParam>>();
        m_EventsFasade.AddEventListener<UiEventFacade.ShowWindowEvent.Param>(OnWindowMustShow);
        m_EventsFasade.AddEventListener<UiEventFacade.HideWindowEvent.Param>(OnWindowMustHide);
    }

    #endregion

    #region WINDOW_MANAGER

    private readonly Dictionary<string, IWindowContext> m_Windows;

    public void RegisterWindow(string windowId, IWindowContext window)
    {
        //Debug.LogFormat("RegisterWindow {0}", windowId);
        if (!m_Windows.ContainsKey(windowId))
        {
            m_Windows.Add(windowId, window);
        }
        else
        {
            Debug.LogErrorFormat("Window with id {0} are already contains in the dictionary.", windowId);
        }
    }

    public void UnRegisterWindow(string windowId)
    {
        if (m_Windows.ContainsKey(windowId))
        {
            //Debug.LogFormat("UnRegisterWindow {0}", windowId);
            m_Windows.Remove(windowId);
        }
        else
        {
            Debug.LogErrorFormat("Window with id {0} was not found in the dictionary.", windowId);
        }
    }

    #endregion

    #region LISTENER

    private PublisherFacade<UiEventFacade.IEventParam> m_EventsFasade;

    private void OnWindowMustShow(UiEventFacade.ShowWindowEvent.Param param)
    {
        //Debug.LogFormat("OnWindowMustShow {0}", message.WindowId);
        if (m_Windows.ContainsKey(param.WindowId))
        {
            IWindowContext window = m_Windows[param.WindowId];
            window.Show(param.Data);
        }
        else
        {
            Debug.LogErrorFormat("Window with id {0} was not found in the dictionary.", param.WindowId);
        }
    }

    private void OnWindowMustHide(UiEventFacade.HideWindowEvent.Param param)
    {
        //Debug.LogErrorFormat("OnWindowMustHide {0}", message.WindowId);
        if (m_Windows.ContainsKey(param.WindowId))
        {
            IWindowContext window = m_Windows[param.WindowId];
            window.Hide(param.Data);
        }
        else
        {
            Debug.LogErrorFormat("Window with id {0} was not found in the dictionary.", param.WindowId);
        }
    }

    #endregion
}

public interface IWindowPrefabsHolder
{
    GameObject GetPrefabByKey(string key);
}

public class WindowManagerCustom : WindowManager, IWindowPrefabsHolder
{
    [Inject]
    private void ResolveDependencies(DiContainer container)
    {
        container.Bind<IWindowPrefabsHolder>().To<WindowManagerCustom>().FromInstance(this);
        m_WindowPrefabsHolder = container.Resolve<WindowPrefabsHolder>();        
    }

    WindowPrefabsHolder m_WindowPrefabsHolder;

    public GameObject GetPrefabByKey(string key)
    {        
        var result = m_WindowPrefabsHolder.Prefabs.FirstOrDefault(x => (x.GetComponent<WindowContextBase>().WindowId == key));

        if (result != null)
        {
            return result;
        }

        throw new System.Exception(string.Format("Can't find prefab for window with id: {0}.", key));
    }


}

public class WindowPrefabsHolder
{
    public WindowPrefabsHolder(DiContainer container)
    {
        Prefabs = new List<GameObject>();
        //Здесь должна быть загрузка префабов окон из ресурсов.
    }

    public List<GameObject> Prefabs { get; private set; }
}