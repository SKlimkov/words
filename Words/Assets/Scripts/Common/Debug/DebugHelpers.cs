﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugHelpers
{
    private static readonly bool isEditor;

    public static bool IsEditor { get { return isEditor; } }

    public static bool IsPlaying
    {
        get
        {
            var result = true;
#if UNITY_EDITOR
            result = UnityEditor.EditorApplication.isPlaying;
#endif
            return result;
        }
    }

    static DebugHelpers()
    {
        isEditor = Application.platform == RuntimePlatform.WindowsEditor;
    }

    public static void Initialize()
    {

    }

    #region COLORED_MESSAGES

    public static Color ColorRed { get { return new Color(0.5294f, 0f, 0f, 1f); } }
    public static Color ColorOrange { get { return new Color(0.7568f, 0.4078f, 0f, 1f); } }
    public static Color ColorGreen { get { return new Color(0.1254f, 0.3843f, 0f, 1f); } }

    public static string GetColoredMessage(Color color, string message)
    {
        return string.Format("<color=#{0:X2}{1:X2}{2:X2}>{3}</color>", (byte)(color.r * 255f), (byte)(color.g * 255f), (byte)(color.b * 255f), message);
    }

    public static string GetRedMessage(string message)
    {
        return GetColoredMessage(ColorRed, message);
    }

    public static string GetRedMessage(string format, params object[] args)
    {
        return GetColoredMessage(ColorRed, string.Format(format, args));
    }

    public static string GetOrangeMessage(string message)
    {
        return GetColoredMessage(ColorOrange, message);
    }

    public static string GetOrangeMessage(string format, params object[] args)
    {
        return GetColoredMessage(ColorOrange, string.Format(format, args));
    }

    public static string GetGreenMessage(string message)
    {
        return GetColoredMessage(ColorGreen, message);
    }

    public static string GetGreenMessage(string format, params object[] args)
    {
        return GetColoredMessage(ColorGreen, string.Format(format, args));
    }

    #endregion

    #region EDITOR_DEBUG_TOOLS

    public static void LogSimpleMessage(string format, params object[] args)
    {
        LogMessage(ColorGreen, format, args);
    }

    public static void LogWarningMessage(string format, params object[] args)
    {
        LogMessage(ColorOrange, format, args);
    }

    public static void LogImportantmMessage(string format, params object[] args)
    {
        LogMessage(ColorRed, format, args);
    }

    private static void LogMessage(Color color, string format, params object[] args)
    {
        if (isEditor)
        {
            var msg = string.Format(format, args);
            Debug.Log(GetColoredMessage(color, msg));
        }
    }

    #endregion
}