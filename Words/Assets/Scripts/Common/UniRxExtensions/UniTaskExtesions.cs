﻿using System;

namespace UniRx.Async.Extensions
{
    public static class UniTaskExtesions
    {
        public static async void Await<TResult>(this IObservable<TResult> observable)
        {
            try
            {
                var result = await observable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}