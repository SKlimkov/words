﻿public class HidingWindowAnimationState : IWindowAnimationState
{
    public void OnAnimationBreak(IWindowAnimationContext context)
    {
        context.OnHidingAnimationAborted();
    }

    public EWindowAnimationState Type { get { return EWindowAnimationState.Hiding; } }
}