﻿using UnityEngine;

namespace CoroutineHandling
{
    public abstract class FixedRoutinableBase<TContext> : IFixedRoutinable
        where TContext : IRoutinableContext
    {
        private TContext m_Context;

        public FixedRoutinableBase(TContext context)
        {
            m_Context = context;
        }

        public virtual void OnRoutinableStart()
        {
            m_Context.OnStart();
        }

        public virtual bool GetConditionCheck()
        {
            return m_Context.OnConditionCheking();
        }

        public virtual void OnAction()
        {
            m_Context.OnAction();
        }

        public virtual void OnFinish()
        {
            m_Context.OnFinish();
        }

        public MonoBehaviour MonoInstance { get { return m_Context.MonoInstance; } }
    }

    public abstract class FrameRoutinableBase<TContext> : IFrameRoutinable
        where TContext : IRoutinableContext
    {
        private TContext m_Context;

        public FrameRoutinableBase(TContext context)
        {
            m_Context = context;
        }

        public virtual void OnRoutinableStart()
        {
            m_Context.OnStart();
        }

        public virtual bool GetConditionCheck()
        {
            return m_Context.OnConditionCheking();
        }

        public virtual void OnAction()
        {
            m_Context.OnAction();
        }

        public virtual void OnFinish()
        {
            m_Context.OnFinish();
        }

        public MonoBehaviour MonoInstance { get { return m_Context.MonoInstance; } }
    }
}