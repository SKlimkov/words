﻿using UnityEngine;
using UnityEngine.U2D;

namespace AtlasSystem
{
    [ExecuteInEditMode]
    public class AtlasRendererSimple : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private ERenderingType m_RenderType;
        [SerializeField] private string m_SpriteName;
        [SerializeField] private SpriteAtlas m_Atlas;
#pragma warning restore 649

        private bool isInitialized;

        private ISpriteWrapperFacade m_WrapperFasade;

        private ISpriteWrapperFacade WrapperFasade
        {
            get
            {
                if (m_WrapperFasade == null)
                {
                    m_WrapperFasade = new SpriteWrapperFacade();
                }

                return m_WrapperFasade;
            }
        }

        private void Awake()
        {
            InitializeSprite();
        }

        private void InitializeSprite()
        {
            if (WrapperFasade == null)
            {
                if (DebugHelpers.IsEditor && !DebugHelpers.IsPlaying)
                {
                    Debug.LogErrorFormat("Can't find wrapper!");
                }
                else
                {
                    throw new System.Exception("Can't find wrapper!");
                }
            }

            if (m_Atlas != null)
            {
                var sprite = m_Atlas.GetSprite(m_SpriteName);
                if (sprite != null)
                {
                    WrapperFasade.SetSprite(m_RenderType, WrapperFasade.GetWrapper(this.gameObject, m_RenderType), sprite);
                }
                else
                {
                    if (DebugHelpers.IsEditor && !DebugHelpers.IsPlaying)
                    {
                        Debug.LogErrorFormat("Can't find sprite!");
                    }
                    else
                    {
                        throw new System.Exception("Can't find sprite!");
                    }
                }
            }
            else
            {
                if (DebugHelpers.IsEditor && !DebugHelpers.IsPlaying)
                {
                    Debug.LogErrorFormat("Can't find atlas!");
                }
                else
                {
                    throw new System.Exception("Can't find atlas!");
                }
            }
        }

#if UNITY_EDITOR
        private string cachedName;
        private void Update()
        {
            if (!DebugHelpers.IsPlaying)
            {
                if (cachedName != m_SpriteName)
                {
                    InitializeSprite();
                }
                cachedName = m_SpriteName;
            }
        }
#endif

        public SpriteAtlas Atlas { get { return m_Atlas; } }
    }
}